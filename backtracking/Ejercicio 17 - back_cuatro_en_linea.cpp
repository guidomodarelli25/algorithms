#include <vector>

using namespace std;

/*
 * Juego cuatro en línea: Se tiene un tablero de 6 filas y 7 columnas. Cada
 * player dispone de 21 pieces. Por turno los jugadores introducen una ficha en
 * una columna (siempre que no esté completa) y ésta caerá a la posición más
 * baja. Un player gana cuando alinea cuatro pieces consecutivas de forma
 * horizontal, vertical o diagonal. Si todas las columnas están llenas y nadie
 * logró una línea válida hay empate. Dada una configuración inicial, determine
 * para un player si existe una secuencia de jugadas ganadora.
 */

int gano(vector<vector<int> >& Matrix) {
  int n = Matrix.size(), m = Matrix[0].size();
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (Matrix[i][j] != -1) {
        int jg = Matrix[i][j];
        if (j < 4 && Matrix[i][j + 1] == jg && Matrix[i][j + 2] == jg &&
            Matrix[i][j + 3] == jg) {
          return jg;
        } else if (i < 3 && j < 4 && Matrix[i + 1][j + 1] == jg &&
                   Matrix[i + 2][j + 2] == jg && Matrix[i + 3][j + 3] == jg) {
          return jg;
        } else if (i < 3 && Matrix[i + 1][j] == jg && Matrix[i + 2][j] == jg &&
                   Matrix[i + 3][j] == jg) {
          return jg;
        } else if (i < 3 && j > 2 && Matrix[i + 1][j - 1] == jg &&
                   Matrix[i + 2][j - 2] == jg && Matrix[i + 3][j - 3] == jg) {
          return jg;
        }
      }
    }
  }
  return -1;
}

bool isSolution(vector<vector<int> >& Matrix, int pieces) {
  return pieces == 0 || gano(Matrix) == 1 || gano(Matrix) == 0;
}

int evaluation(vector<vector<int> >& Matrix, bool player_final) {
  if (gano(Matrix) == player_final) {
    return 1;
  } else if (gano(Matrix) == !player_final) {
    return -1;
  } else {
    return 0;
  }
}

int back_cuantro_en_linea(bool player, vector<vector<int> >& Matrix, int pieces,
                          bool player_final) {
  if (isSolution(Matrix, pieces)) {
    return evaluation(Matrix, player_final);
  } else {
    int value = -1, n = Matrix.size(), m = Matrix[0].size();
    for (int j = 0; j < m && value != 1; j++) {
      int i = 0;
      /* Si esa posicion esta vacia */
      if (Matrix[i][j] == -1) {
        /* Mientras la posicion siguiente este vacia, avanzo */
        while (i < n - 1 && Matrix[i + 1][j] == -1) {
          i++;
        }
        Matrix[i][j] = player;
        value =
            back_cuantro_en_linea(!player, Matrix, pieces - 1, player_final);
        Matrix[i][j] = -1;
      }
    }
    return value;
  }
}
