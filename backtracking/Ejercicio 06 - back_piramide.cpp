#include <vector>

using namespace std;

/*
 * Colocar un entero positivo (menor que 100) en cada casilla de una piramide de
 * modo que cada numero sea igual a la suma de las casillas sobre las que esta
 * apoyado. Los numeros de todas las casillas deben ser diferentes.
 */

bool checkEquals(vector<vector<int> >& Matrix, vector<bool> used, int nro) {
  int n = Matrix.size();
  for (int i = 1; i < n; i++) {
    for (int j = i; j < n; j++) {
      if (Matrix[i - 1][j] == 0) {
        return false;
      }
      int value = Matrix[i - 1][j - 1] + Matrix[i - 1][j];
      if (value >= nro || used[value]) {
        return true;
      }
      Matrix[i][j] = value;
      used[value] = true;
    }
  }
  return false;
}

int back_piramide(vector<vector<int> >& Matrix, int k, vector<bool>& used,
                  int nro) {
  int n = Matrix.size();
  if (k == n) {
    // show(Matrix);
    return 1;
  } else {
    int solution = 0;
    for (int value = k + 1; value < 20 && solution == 0; value++) {
      if (!used[value]) {
        Matrix[0][k] = value;
        used[value] = true;
        if (!checkEquals(Matrix, vector<bool>(used), nro)) {
          solution = back_piramide(Matrix, k + 1, used, nro);
        }
        used[value] = false;
        Matrix[0][k] = 0;
      }
    }
    return solution;
  }
}
