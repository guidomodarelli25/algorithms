#include <algorithm>
#include <vector>

using namespace std;

/*
 * Asignacion de tareas a processors. Se tienen m processors identicos y n
 * tareas con un tiempo de ejecucion dado. Se requiere encontrar una asignacion
 * de tareas a processors de manera de minimizar el tiempo de ejecucion del
 * total de tareas.
 */

void back_asignacionProcesadores(int k, vector<int>& times,
                                 vector<int>& best_assignment,
                                 vector<int>& current_assignment,
                                 int& totalTime, int partialTime,
                                 vector<int>& processors) {
  int n = times.size(), m = processors.size();
  if (k == n) {
    if (totalTime > partialTime) {
      best_assignment = current_assignment;
      totalTime = partialTime;
    }
  } else {
    if (n <= m) {
      for (int i = 1; i <= n; i++) {
        best_assignment[i] = i;
      }
      return;
    }
    for (int i = 1; i <= m; i++) {
      /* A la tarea k le asigno el procesador i */
      current_assignment[k] = i;
      /* Al procesador i le sumo el tiempo de la tarea k */
      processors[i] += times[k];
      back_asignacionProcesadores(
          k + 1, times, best_assignment, current_assignment, totalTime,
          *max_element(processors.begin(), processors.end()), processors);
      current_assignment[k] = 0; /* deshago la asignacion */
      processors[i] -= times[k];
    }
  }
}
