#include <vector>

using namespace std;

/*
 * Particion de Set. Dado un Set de n enteros se desea encontrar, si
 * existe, una particion en dos subconjuntos disjuntos, tal que la suma de sus
 * elementos sea la misma.
 */

int back_particionConjuntos(vector<int>& Set, vector<int>& subSet, int k,
                            int totalSum, int partialSum) {
  if (partialSum == totalSum / 2) {
    return 1;
  } else {
    if (totalSum % 2 != 0) {
      return 0;
    }
    int solution = 0;
    for (unsigned i = k; i < Set.size() && solution == 0; i++) {
      subSet[k] = Set[i];
      if (partialSum <= totalSum / 2) {
        solution = back_particionConjuntos(Set, subSet, k + 1, totalSum,
                                           partialSum + Set[i]);
      }
      subSet[k] = 0;
    }
    return solution;
  }
}
