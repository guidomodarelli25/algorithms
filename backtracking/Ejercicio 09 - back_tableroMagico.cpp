#include <vector>

using namespace std;

/*
 * Tablero magico. Dado un tablero de tamaño n x n, construir un algoritmo que
 * ubique (si es posible) n x n numeros naturales diferentes, entre 1 y un
 * cierto k, de manera tal que la suma de las columnas y de las filas sea S.
 */

bool addColumns(vector<int>& result, int nroBox, int s) {
  int n = result.size();
  if ((nroBox + 1) % n == 0) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
      sum += result[nroBox - i];
    }
    if (sum != s) {
      return true;
    }
  }
  return false;
}

bool addRows(vector<int>& result, int nroBox, int s) {
  int n = result.size();
  if (nroBox + n >= n * n) {
    int sum = 0;
    for (int i = 0; i <= nroBox; i += n) {
      sum += result[nroBox - i];
    }
    if (sum != s) {
      return true;
    }
  }
  return false;
}

bool poda(vector<int>& result, int nroBox, int s) {
  return addColumns(result, nroBox, s) || addRows(result, nroBox, s);
}

int back_tableroMagico(vector<int>& result, int s, int k, int nroBox,
                       vector<bool>& used) {
  int n = result.size();
  if (nroBox == n * n) {
    return 1;
  } else {
    int solution = 0;
    for (int i = 1; i < k && solution == 0; i++) {
      if (!used[i]) {
        result[nroBox] = i;
        used[i] = true;
        if (!poda(result, nroBox, s)) {
          solution = back_tableroMagico(result, s, k, nroBox + 1, used);
        }
        result[nroBox] = 0;
        used[i] = false;
      }
    }
    return solution;
  }
}
