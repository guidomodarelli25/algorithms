#include <vector>

using namespace std;

/*
 * Salto del caballo. Encontrar un sucesion de movs "legales" de un
 * caballo de ajedrez de forma tal que este pueda visitar todas y cada una de
 * las casillas de un tablero sin repetir ninguna. Variante: Obligar al caballo
 * a volver a la posicion de partida en el ultimo mov.
 */

int getI(int u, int m) { return u / m; }

int getJ(int u, int m) { return u - (u / m) * m; }

int move(int i, int j, int mov, int m) {
  switch (mov) {
    case 0:
      return j + i * m - 2 * m + 1;
    case 1:
      return j + i * m - m + 2;
    case 2:
      return j + i * m + m + 2;
    case 3:
      return j + i * m + 2 * m + 1;
    case 4:
      return j + i * m + 2 * m - 1;
    case 5:
      return j + i * m + m - 2;
    case 6:
      return j + i * m - m - 2;
    default:
      return j + i * m - 2 * m - 1;
  }
}

bool validMovement(int mov, int n, int m, int i, int j) {
  switch (mov) {
    case 0:
      return i > 1 && j < m - 1;
    case 1:
      return i > 0 && j < m - 2;
    case 2:
      return i < n - 1 && j < m - 2;
    case 3:
      return i < n - 2 && j < m - 1;
    case 4:
      return i < n - 2 && j > 0;
    case 5:
      return i < n - 1 && j > 1;
    case 6:
      return i > 0 && j > 1;
    default:
      return i > 1 && j > 0;
  }
}

int back_saltoCaballo(vector<int>& result, vector<int>& visited, int n, int m,
                      int u, int k) {
  visited[u] = true;
  if (k == n * m) {
    return 1;
  } else {
    int solution = 0;
    int i = getI(u, m), j = getJ(u, m);
    for (int mov = 0; mov < 8 && solution == 0; mov++) {
      int v = move(i, j, mov, m);
      if (validMovement(mov, n, m, i, j) && !visited[v]) {
        result[k] = v;
        solution = back_saltoCaballo(result, visited, n, m, v, k + 1);
        visited[v] = false;
        result[k] = 0;
      }
    }
    return solution;
  }
}
