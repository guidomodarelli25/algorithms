#include <climits>
#include <list>
#include <vector>

using namespace std;

/*
 * Dado un tablero de 4 x 4, en cuyas casillas se encuentran desordenados los
 * numero enteros de 1 al 15 y una casilla desocupada en cualquier posicion,
 * determinar una secuencia de pasos tal que los numeros en el tablero queden
 * ordenados y la casilla desocupada quede en la posicion 4,4.
 */

int getI(int u, int n) { return u / n; }

int getJ(int u, int n) { return u - (u / n) * n; }

void modify(vector<vector<int> >& M, int i, int j, int k, int l, bool h) {
  if (h) {
    M[i + k][j + l] = M[i][j];
    M[i][j] = INT_MAX;
  } else {
    M[i][j] = M[i + k][j + l];
    M[i + k][j + l] = INT_MAX;
  }
}

int move(vector<vector<int> >& M, int mov, int i, int j, bool h) {
  int n = M.size();
  int u = j + i * n + 1;
  switch (mov) {
    case 0:
      modify(M, i, j, -1, 0, h);
      return u - n;
    case 1:
      modify(M, i, j, 0, 1, h);
      return u + 1;
    case 2:
      modify(M, i, j, 1, 0, h);
      return u + n;
    default:
      modify(M, i, j, 0, -1, h);
      return u - 1;
  }
}

bool validMovement(int mov, int i, int j, int n) {
  switch (mov) {
    case 0:
      return i > 0;
    case 1:
      return j < n - 1;
    case 2:
      return i < n - 1;
    default:
      return j > 0;
  }
}

bool equals(vector<vector<int> >& A, vector<vector<int> >& B) {
  int n = A.size();
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (A[i][j] != B[i][j]) {
        return false;
      }
    }
  }
  return true;
}

bool itsACycle(vector<vector<int> >& M, list<vector<vector<int> > >& movs) {
  for (list<vector<vector<int> > >::iterator m = movs.begin(); m != movs.end();
       m++) {
    if (equals(M, *m)) {
      return true;
    }
  }
  return false;
}

bool esSolucion(vector<vector<int> >& M) {
  int n = M.size();
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (((j + 1) + i * n) != (n * n)) {
        if (M[i][j] != (((j + 1) + i * n) % (n * n))) {
          return false;
        }
      } else if (M[i][j] != INT_MAX) {
        return false;
      }
    }
  }
  return true;
}

int back_puzzleN2(vector<vector<int> >& M, int u,
                  list<vector<vector<int> > >& movs) {
  if (esSolucion(M)) {
    return 1;
  } else {
    movs.push_back(M);
    int solution = 0, n = M.size();
    int i = getI(u, n), j = getJ(u, n);
    for (int mov = 0; mov < 4 && solution == 0; mov++) {
      if (validMovement(mov, i, j, n)) {
        int v = move(M, mov, i, j, 0);
        if (!itsACycle(M, movs)) {
          solution = back_puzzleN2(M, v, movs);
          movs.pop_back();
        }
        move(M, mov, i, j, 1); /* deshace el mov */
      }
    }
    return solution;
  }
}
