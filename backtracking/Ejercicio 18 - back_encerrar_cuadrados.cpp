#include <vector>

using namespace std;

/*
 * Juego encerrar cuadrados: Se tiene un tablero con puntos en forma de
 * cuadrícula. El objetivo del juego es completar cuadrados y apoderarse de la
 * mayor quantity de éstos posibles. En cada jugada, de forma alternada, un
 * player unirá dos puntos consecutivos horizontal o verticalmente, de esta
 * forma, los cuadrados se van formando lado por lado. Cuando un player
 * forma con estas líneas un cuadrado, se apodera del mismo (marcándolo con
 * la inicial del player). Después de formar un cuadrado está obligado a
 * seguir jugando. El player que obtenga más cuadrados gana la partida.
 * Dada una configuración inicial, determine para un player si existe una
 * secuencia de jugadas ganadora.
 */

int getI(int k, int n) { return k / n; }

int getJ(int k, int n) { return k - getI(k, n) * n; }

int numberOfSquares(vector<vector<char> >& Matrix, char player_final) {
  int quantity = 0, n = Matrix.size(), m = Matrix[0].size();
  for (int i = 1; i < n; i += 2) {
    for (int j = 1; j < m; j += 2) {
      if (Matrix[i][j] == player_final) {
        quantity++;
      }
    }
  }
  return quantity;
}

pair<int, int> fullSquares(vector<vector<char> >& Matrix, int i, int j) {
  int n = Matrix.size(), m = Matrix[0].size();
  pair<int, int> p = make_pair(0, 0);
  if (j >= 1 && j <= m - 2 && Matrix[i][j - 1] == '*' &&
      Matrix[i][j + 1] == '*') {
    if (i >= 2 && Matrix[i - 1][j] == ' ' && Matrix[i - 1][j - 1] != ' ' &&
        Matrix[i - 2][j] != ' ' && Matrix[i - 1][j + 1] != ' ') {
      p.first = j + (i - 1) * m;
    }
    if (i <= n - 3 && Matrix[i + 1][j] == ' ' && Matrix[i + 1][j - 1] != ' ' &&
        Matrix[i + 2][j] != ' ' && Matrix[i + 1][j + 1] != ' ') {
      p.second = j + (i + 1) * m;
    }
  } else {
    if (j >= 2 && Matrix[i][j - 1] == ' ' && Matrix[i - 1][j - 1] != ' ' &&
        Matrix[i][j - 2] != ' ' && Matrix[i + 1][j - 1] != ' ') {
      p.first = (j - 1) + i * m;
    }
    if (j <= m - 3 && Matrix[i][j + 1] == ' ' && Matrix[i - 1][j + 1] != ' ' &&
        Matrix[i][j + 2] != ' ' && Matrix[i + 1][j + 1] != ' ') {
      p.second = (j + 1) + i * m;
    }
  }
  return p;
}

void markSquare(vector<vector<char> >& Matrix, pair<int, int> p, char mark) {
  int m = Matrix[0].size();
  if (p.first != 0) {
    Matrix[getI(p.first, m)][getJ(p.first, m)] = mark;
  }
  if (p.second != 0) {
    Matrix[getI(p.second, m)][getJ(p.second, m)] = mark;
  }
}

char changePlayer(char player) {
  if (player == 'a') {
    return 'b';
  } else {
    return 'a';
  }
}

bool isSolution(vector<vector<char> >& Matrix, char player_final,
                int quantity) {
  return numberOfSquares(Matrix, player_final) >
             quantity + numberOfSquares(Matrix, changePlayer(player_final)) ||
         numberOfSquares(Matrix, changePlayer(player_final)) >
             quantity + numberOfSquares(Matrix, player_final) ||
         quantity == 0;
}

int evaluation(vector<vector<char> >& Matrix, char player_final, int quantity) {
  if (numberOfSquares(Matrix, player_final) >
      quantity + numberOfSquares(Matrix, changePlayer(player_final))) {
    return 1;
  } else if (numberOfSquares(Matrix, changePlayer(player_final)) >
             quantity + numberOfSquares(Matrix, player_final)) {
    return -1;
  } else {
    return 0;
  }
}

int back_enclose_squares(vector<vector<char> >& Matrix, int quantity,
                         char player, char player_final) {
  if (isSolution(Matrix, player_final, quantity)) {
    return evaluation(Matrix, player_final, quantity);
  } else {
    int value = -1, n = Matrix.size(), m = Matrix[0].size();
    for (int k = 1; k < n * m && value != 1; k += 2) {
      int i = getI(k, m), j = getJ(k, m);
      if (Matrix[i][j] == ' ') {
        Matrix[i][j] = player;
        pair<int, int> p = fullSquares(Matrix, i, j);
        if (p.first != 0 || p.second != 0) {
          markSquare(Matrix, p, player);
          int cuadrados = 0;
          if (p.first != 0) {
            cuadrados++;
          }
          if (p.second != 0) {
            cuadrados++;
          }
          value = back_enclose_squares(Matrix, quantity - cuadrados, player,
                                       player_final);
          markSquare(Matrix, p, ' ');
        } else {
          value = back_enclose_squares(Matrix, quantity, changePlayer(player),
                                       player_final);
        }
        Matrix[i][j] = ' ';
      }
    }
    return value;
  }
}
