#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

/**
 * It checks if the number k is in the vector solution
 *
 * @param solution the current solution vector
 * @param k the number we're trying to find
 *
 * @return true if k is in the vector solution, false otherwise
 */
bool areEquals(vector<int>& solution, int k) {
  for (int i = 0; i < solution.size() - 1; i++) {
    if (solution[i] == k) {
      return true;
    }
  }
  return false;
}

bool isAdjacent(Graph& g, vector<int>& solution, int k) {
  if (solution.size() > 2) {
    list<Graph::Arch> adjacents = g.getAdjacents(k);
    for (int i = 0; i < solution.size() - 2; i++) {
      int equals = false;
      for (Graph::Arch adjacent : adjacents) {
        if (solution[i] == adjacent.getAdjacent()) {
          equals = true;
          break;
        }
      }
      if (equals) return true;
    }
  } else {
    return true;
  }
  return false;
}

bool poda(Graph& g, int k, vector<int>& solution) {
  return areEquals(solution, k) || !isAdjacent(g, solution, k);
}

void back_clique(vector<int>& solution, Graph& g, int k,
                 vector<int>& best_solution) {
  list<Graph::Arch> adjacents = g.getAdjacents(k);
  for (Graph::Arch adjacent : adjacents) {
    solution.push_back(adjacent.getAdjacent());
    if (!poda(g, adjacent.getAdjacent(), solution)) {
      if (solution.size() >= 2) {
        if (solution.size() > best_solution.size()) {
          best_solution = solution;
        }
      }
      back_clique(solution, g, adjacent.getAdjacent(), best_solution);
    }
    solution.pop_back();
  }
}
