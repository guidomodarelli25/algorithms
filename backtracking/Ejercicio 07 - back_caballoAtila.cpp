#include <vector>

using namespace std;

/*
 * Caballo de Atila. Todos sabemos que por donde pisa el caballo de Atila jamas
 * vuelve a crecer el pasto. El caballo fue directamente hacia el jardin de
 * n x n casillas. Empezó su paseo por una casilla cualquiera y volvio a ella,
 * es decir, hizo un recorrido cerrado. No visito dos veces una misma casilla,
 * se movio de una casilla a otra vecina en forma horinzontal o vertical, pero
 * nunca en diagonal. Por donde piso el caballo, el pasto jamas volvio a crecer.
 * Luego de terminado el recorrido en algunas casillas todavia habia pasto
 * (señal de que en ellas no habia estado el caballo). Escriba un algoritmo que
 * deduzca el recorrido completo que hizo el caballo.
 */

int getI(int u, int m) { return u / m; }

int getJ(int u, int m) { return u - (u / m) * m; }

int move(int n, int i, int j, int mov) {
  switch (mov) {
    case 0:
      return j + i * n - n;
    case 1:
      return j + i * n + 1;
    case 2:
      return j + i * n + n;
    default:
      return j + i * n - 1;
  }
}

bool validMovement(vector<vector<int> >& Matrix, int i, int j, int mov) {
  int n = Matrix.size();
  switch (mov) {
    case 0:
      return i > 0 && Matrix[i - 1][j] == 0;
    case 1:
      return j < n - 1 && Matrix[i][j + 1] == 0;
    case 2:
      return i < n - 1 && Matrix[i + 1][j] == 0;
    default:
      return j > 0 && Matrix[i][j - 1] == 0;
  }
}

bool theyAreNeighbours(int n, int u, int inicial) {
  return u - n == inicial || u + 1 == inicial || u + n == inicial ||
         u - 1 == inicial;
}

int back_caballoAtila(vector<vector<int> >& Matrix, int u, vector<int>& visited,
                      vector<int>& result, int steppedSquares, int k) {
  int n = Matrix.size();
  visited[u] = true;
  if (k == steppedSquares && theyAreNeighbours(n, u, result[0])) {
    return 1;
  } else {
    int solution = 0;
    int i = getI(u, n), j = getJ(u, n);
    for (int mov = 0; mov < 4 && solution == 0; mov++) {
      int v = move(n, i, j, mov);
      if (validMovement(Matrix, i, j, mov) && !visited[v]) {
        result[k] = v;
        solution = back_caballoAtila(Matrix, v, visited, result, steppedSquares,
                                     k + 1);
        visited[v] = false;
        result[k] = 0;
      }
    }
    return solution;
  }
}
