#include <list>
#include <vector>

using namespace std;

/*
 * Se tienen seis crocks ubicadas sobre piedras, tres tanas blancas a la
 * izquierda y tres tanas grises a la derecha, en donde cada rana puede avanzar
 * en el sentido hacia donde mira de a una piedra a la vez o saltar otra rana
 * (siempre y cuando existe un espacio libre justo detras de la rana que esta
 * siendo saltada). Escriba un algoritmo que deduzca la secuencia de
 * movimientos que se deben realizar para llevar las crocks blancas al extremo
 * derecho y las crocks grises al izquierdo.
 */

typedef pair<int, int> Play;

void move(vector<int>& crocks, int mov, int u, int h, int destiny) {
  if (!h) {
    if (crocks[u] == 1) {
      crocks[u + mov] = 1;
    } else {
      crocks[u - mov] = -1;
    }
    crocks[u] = 0;
  } else {
    if (crocks[destiny] == 1) {
      crocks[u] = 1;
    } else {
      crocks[u] = -1;
    }
    crocks[destiny] = 0;
  }
}

bool validMovement(vector<int>& crocks, int mov, int u) {
  int n = crocks.size();
  return (crocks[u] == 1 && u < n - mov && crocks[u + mov] == 0) ||
         (crocks[u] == -1 && u >= mov && crocks[u - mov] == 0);
}

bool isSolution(vector<int>& crocks) {
  int n = crocks.size();
  for (int i = 0; i < n; i++) {
    if (i < n / 2 && crocks[i] != -1) {
      return false;
    }
    if (i == n / 2 && crocks[i] != 0) {
      return false;
    }
    if (i > n / 2 && crocks[i] != 1) {
      return false;
    }
  }
  return true;
}

int back_ranas(vector<int>& crocks, list<Play>& movs) {
  if (isSolution(crocks)) {
    return 1;
  } else {
    int solucion = 0, n = crocks.size();
    for (int rana = 0; rana < n; rana++) {
      for (int mov = 1; mov < 3 && solucion == 0; mov++) {
        if (validMovement(crocks, mov, rana)) {
          if (crocks[rana] == 1) {
            movs.push_back(make_pair(rana, rana + mov));
          } else {
            movs.push_back(make_pair(rana, rana - mov));
          }
          move(crocks, mov, rana, 0, movs.back().second);
          solucion = back_ranas(crocks, movs);
          movs.pop_back();
          move(crocks, mov, rana, 1, movs.back().second);
        }
      }
    }
    return solucion;
  }
}
