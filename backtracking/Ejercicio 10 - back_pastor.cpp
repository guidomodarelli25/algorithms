#include <list>
#include <vector>

using namespace std;

/*
 * Un pastor debe cruzar un rio trasladando de un lado a otro a una oveja, un
 * fardo y un leon. se debe tener en cuenta que si el hombre no esta sucede lo
 * siguiente: la oveja se come al fardo, y el leon se come a la oveja. ¿Como
 * debe hacer el hombre para cruzar el rio si cuenta con un bote con el cual
 * puede trasladar de a un elemento por vez?
 */

#define LION 0
#define BALE 1
#define SHEEP 2
#define SHEPHERD 3

bool poda(vector<bool>& result) {
  if (result[SHEPHERD] != result[BALE] && result[BALE] == result[SHEEP]) {
    return true;
  }
  if (result[SHEPHERD] != result[LION] && result[LION] == result[SHEEP]) {
    return true;
  }
  return false;
}

bool equals(vector<bool>& A, vector<bool>& B) {
  int n = A.size();
  for (int i = 0; i < n; i++) {
    if (A[i] != B[i]) {
      return false;
    }
  }
  return true;
}

bool itsAcycle(list<vector<bool> >& travel, vector<bool>& result) {
  for (list<vector<bool> >::iterator v = travel.begin(); v != travel.end();
       v++) {
    if (equals(*v, result)) {
      return true;
    }
  }
  return false;
}

void move(vector<bool>& result, int i) {
  if (i != -1) {
    result[i] = !result[i];
  }
  result[SHEPHERD] = !result[SHEPHERD];
}

int back_pastor(vector<bool>& result, list<vector<bool> >& travel) {
  int n = result.size();
  travel.push_back(result);
  if (result[LION] && result[BALE] && result[SHEEP] && result[SHEPHERD]) {
    return 1;
  } else {
    int solution = 0;
    for (int i = -1; i < 3 && solution == 0; i++) {
      move(result, i);
      if (!poda(result) && !itsAcycle(travel, result)) {
        solution = back_pastor(result, travel);
        travel.pop_back();
      }
      move(result, i);
    }
    return solution;
  }
}
