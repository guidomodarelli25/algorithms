#include <algorithm>
#include <vector>

using namespace std;

/*
 * Ocho queens. Ubique ocho queens en un tablero de ajedrez tal que no se puedan
 * capturar entre sí.
 */

bool poda(vector<int>& queens, int i) {
  for (int j = 0; j < i; j++) {
    if (queens[i] == queens[j] || i - j == abs(queens[i] - queens[j]))
      return true;
  }
  return false;
}

int back_NReinas(vector<int>& queens, int nroQueen) {
  if (nroQueen == queens.size()) {
    return 1;
  } else {
    int solution = 0;
    for (int c = 0; c < queens.size() && solution == 0; c++) {
      queens[nroQueen] = c;
      if (!poda(queens, nroQueen)) {
        solution = back_NReinas(queens, nroQueen + 1);
      }
      queens[nroQueen] = -1;
    }
    return solution;
  }
}
