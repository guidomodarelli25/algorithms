#include <vector>

using namespace std;

/*
 * Dado un laberinto consistente en una matriz cuadrada que tiene en cada
 * posicion un value natural y cuatro valores booleanos, indicando estos ultimos
 * si desde esa casilla se puede ir al north, east, south y west, encontrar un
 * camino de longitud minima entre dos casillas dadas, siendo la longitud de un
 * camino la suma de los valores naturales de las casillas por las que pasa.
 */

struct node {
  int value;
  bool north;
  bool east;
  bool south;
  bool west;
};

int getI(int u, int n) { return u / n; }

int getJ(int u, int n) { return u - (u / n) * n; }

int move(int n, int i, int j, int mov) {
  switch (mov) {
    case 0:
      return j + i * n - n;
    case 1:
      return j + i * n + 1;
    case 2:
      return j + i * n + n;
    default:
      return j + i * n - 1;
  }
}

bool validMovement(vector<vector<node> >& Matriz, int mov, int i, int j,
                   int n) {
  switch (mov) {
    case 0:
      return Matriz[i][j].north;
    case 1:
      return Matriz[i][j].east;
    case 2:
      return Matriz[i][j].south;
    default:
      return Matriz[i][j].west;
  }
}

void back_laberinto(vector<vector<node> >& Matriz, int u, int output,
                    vector<bool>& visited, vector<int>& best_solution,
                    vector<int>& current_solution, int& totalSum,
                    int partialSum, int k) {
  visited[u] = true;
  if (u == output) {
    if (totalSum > partialSum) {
      best_solution = current_solution;
      totalSum = partialSum;
    }
  } else {
    int n = Matriz.size();
    int i = getI(u, n), j = getJ(u, n);
    for (int mov = 0; mov < 4; mov++) {
      int v = move(n, i, j, mov);
      if (validMovement(Matriz, mov, i, j, n) && !visited[v]) {
        current_solution[k] = v;
        back_laberinto(
            Matriz, v, output, visited, best_solution, current_solution,
            totalSum, partialSum + Matriz[getI(v, n)][getJ(v, n)].value, k + 1);
        visited[v] = false;
        current_solution[k] = 0;
      }
    }
  }
}
