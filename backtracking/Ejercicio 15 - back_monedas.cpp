#include <vector>

using namespace std;

/*
 * Pilas de monedas: Dada una pila de n monedas, y 2 jugadores, quienes
 * alternan movimientos, cada uno de ellos puede dividir una pila en otras 2,
 * siempre y cuando haya mas de 2 monedas. Pierde el player que no puede
 * dividir ninguna pila. Construir un algoritmo que determine para un player
 * si existe una secuencia de jugadas ganadora.
 */

bool isSolution(vector<int>& stack_coins) {
  for (unsigned i = 0; i < stack_coins.size(); i++) {
    if (stack_coins[i] > 2) {
      return false;
    }
  }
  return true;
}

int back_monedas(bool player, vector<int>& stack_coins, bool player_final) {
  if (isSolution(stack_coins)) {
    if (!player == player_final) {
      return 1;
    } else {
      return -1;
    }
  } else {
    int value = -1;
    for (unsigned i = 0; i < stack_coins.size() && value != 1; i++) {
      if (stack_coins[i] > 2) {
        for (int c = 1; c < stack_coins[i] && value != 1; c++) {
          stack_coins[i] -= c;
          stack_coins.push_back(c);
          value = back_monedas(!player, stack_coins, player_final);
          stack_coins.pop_back();
          stack_coins[i] += c;
        }
      }
    }
    return value;
  }
}
