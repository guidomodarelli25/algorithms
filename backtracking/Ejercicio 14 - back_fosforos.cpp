#include <vector>

using namespace std;

/*
 * Juego de los fosforos: Se disponen de 11 fosforos. Dos jugadores pueden
 * retirar por turno 1, 2 o 3 fosforos. Pierde aquel player que retira el
 * ultimo fosforo. Construir un algoritmo que determine para un player si
 * existe una secuencia de jugadas ganadora.
 */

int back_fosforos(bool player, int quantity, bool player_final) {
  if (quantity == 1) {
    if (!player == player_final) {
      return 1;
    } else {
      return -1;
    }
  } else {
    int value = -1;
    for (int i = 1; i < 4 && value != 1; i++) {
      if (quantity - i > 0) {
        value = back_fosforos(!player, quantity - i, player_final);
      }
    }
    return value;
  }
}
