#include <vector>

using namespace std;

/*
 * Juego del Ta-Te-Ti: Implemente un algoritmo que determine para un player si
 * existe una secuencia de jugadas ganadora. Un player gana si consigue tener
 * una linea de tres de sus simbolos, la linea puede ser horizontal, vertical o
 * diagonal.
 */

int getI(int k, int n) { return k / n; }

int getJ(int k, int n) { return k - (k / n) * n; }

int gano(vector<vector<int> >& Matrix) {
  int n = Matrix.size();
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if (Matrix[i][j] != -1) {
        int jg = Matrix[i][j];
        if (j < 1 && Matrix[i][j + 1] == jg && Matrix[i][j + 2] == jg) {
          return jg;
        } else if (i < 1 && j < 1 && Matrix[i + 1][j + 1] == jg &&
                   Matrix[i + 2][j + 2] == jg) {
          return jg;
        } else if (i < 1 && Matrix[i + 1][j] == jg && Matrix[i + 2][j] == jg) {
          return jg;
        } else if (i < 1 && j > 1 && Matrix[i + 1][j - 1] == jg &&
                   Matrix[i + 2][j - 2] == jg) {
          return jg;
        }
      }
    }
  }
  return -1;
}

bool isSolution(vector<vector<int> >& Matrix, int pieces) {
  return pieces == 0 || gano(Matrix) == 1 || gano(Matrix) == 0;
}

int evaluacion(vector<vector<int> >& Matrix, bool player_final) {
  if (gano(Matrix) == player_final) {
    return 1;
  } else if (gano(Matrix) == !player_final) {
    return -1;
  } else {
    return 0;
  }
}

int back_Ta_Te_Ti(bool player, vector<vector<int> >& Matrix, int pieces,
                  bool player_final) {
  if (isSolution(Matrix, pieces)) {
    return evaluacion(Matrix, player_final);
  } else {
    int valor = -1, n = Matrix.size();
    for (int k = 0; k < n * n && valor != 1; k++) {
      int i = getI(k, n), j = getJ(k, n);
      if (Matrix[i][j] == -1) {
        Matrix[i][j] = player;
        valor = back_Ta_Te_Ti(!player, Matrix, pieces - 1, player_final);
        Matrix[i][j] = -1;
      }
    }
    return valor;
  }
}
