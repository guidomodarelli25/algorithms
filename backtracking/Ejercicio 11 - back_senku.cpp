#include <list>
#include <vector>

using namespace std;

/*
 * Dado un tablero en forma de cruz de lado 3, en el cual se encuentran
 * distribuidas 44 fichas quedando libre el centro del mismo, el juego consiste
 * en dejar solo una piece en la posicion central del tablero. Una piece puede
 * saltar sobre otra (en cualquiera de los cuatro sentidos) y caer en un lugar
 * libre debiendo eliminarse la piece sobre la que se salto (Juego del senku).
 * Determinar secuencua de steps tal que logre el objetivo del juego.
 */

typedef pair<int, int> Play;

int getI(int piece, int n) { return piece / n; }

int getJ(int piece, int n) { return piece - (piece / n) * n; }

int move(vector<vector<int> >& Matrix, int mov, int i, int j, bool h) {
  int n = Matrix.size();
  Matrix[i][j] = h;
  if (mov == 0) {
    Matrix[i - 1][j] = h;
    Matrix[i - 2][j] = !h;
    return j + i * n - 2 * n;
  } else if (mov == 1) {
    Matrix[i][j + 1] = h;
    Matrix[i][j + 2] = !h;
    return j + i * n + 2;
  } else if (mov == 2) {
    Matrix[i + 1][j] = h;
    Matrix[i + 2][j] = !h;
    return j + i * n + 2 * n;
  } else if (mov == 3) {
    Matrix[i][j - 1] = h;
    Matrix[i][j - 2] = !h;
    return j + i * n - 2;
  }
}

bool validMovement(vector<vector<int> >& Matrix, int mov, int i, int j) {
  if (mov == 0) {
    if (((j <= 2 || j >= 6) && i == 5) || (j > 2 && j < 6 && i >= 2)) {
      return Matrix[i - 1][j] == 1 && Matrix[i - 2][j] == 0;
    }
  } else if (mov == 1) {
    if (((i <= 2 || i >= 6) && j == 3) || (i > 2 && i < 6 && j <= 6)) {
      return Matrix[i][j + 1] == 1 && Matrix[i][j + 2] == 0;
    }
  } else if (mov == 2) {
    if (((j <= 2 || j >= 6) && i == 3) || (j > 2 && j < 6 && i <= 6)) {
      return Matrix[i + 1][j] == 1 && Matrix[i + 2][j] == 0;
    }
  } else if (mov == 3) {
    if (((i <= 2 || i >= 6) && j == 5) || (i > 2 && i < 6 && j >= 2)) {
      return Matrix[i][j - 1] == 1 && Matrix[i][j - 2] == 0;
    }
  }
  return false;
}

int back_senku(vector<vector<int> >& Matrix, int quantity, list<Play>& steps) {
  if (quantity == 1 && Matrix[4][4] == 1) {
    return 1;
  } else {
    int solution = 0, n = Matrix.size();
    for (int piece = 0; piece < n * n && solution == 0; piece++) {
      int i = getI(piece, n), j = getJ(piece, n);
      if (Matrix[i][j] == 1) {
        for (int mov = 0; mov < 4 && solution == 0; mov++) {
          if (validMovement(Matrix, mov, i, j)) {
            int v = move(Matrix, mov, i, j, 0);
            steps.push_back(make_pair(piece, v));
            solution = back_senku(Matrix, quantity - 1, steps);
            steps.pop_back();
            move(Matrix, mov, i, j, 1);
          }
        }
      }
    }
    return solution;
  }
}
