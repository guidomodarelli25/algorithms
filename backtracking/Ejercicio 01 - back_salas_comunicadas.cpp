#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

/*
 * Se tiene un conjunto de salas comunicadas entre sí a través de puertas que
 * se abren solamente en un sentido. Una de las salas se denomina entrada y la
 * otra salida. Construir un algoritmo que permita ir desde la entrada a la
 * salida atravesando la máxima cantidad de salas.
 */

void back_salas_comunicadas(Graph& g, int u, int output, vector<bool>& visited,
                            vector<int>& best_solution,
                            vector<int>& current_solution, int k, int& kTotal) {
  visited[u] = true;
  if (current_solution[k] == output) {
    if (k > kTotal) {
      best_solution = current_solution;
      kTotal = k;
    }
  } else {
    vector<int> adj = g.getAdjacentsVertices(u);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      if (!visited[v]) {
        current_solution[k] = v;
        back_salas_comunicadas(g, v, output, visited, best_solution,
                               current_solution, k + 1, kTotal);
        visited[v] = false;
        current_solution[k] = 0;
      }
    }
  }
}
