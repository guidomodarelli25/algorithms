#include <vector>

using namespace std;

/* Best Fit */

/*
 * Los objetos se colocan en aquellos cajones que caben pero que mas llenos
 * estan.
 */

void bestFit(vector<int>& S, vector<int>& B, vector<int>& I, int N) {
  for (int i = 0; i < N; i++) {
    int k = -1;
    for (int j = 0; j < N; j++) {
      if (B[j] + S[i] <= 10) {
        if (k == -1 || B[k] < B[j]) {
          k = j;
        }
      }
    }
    if (k != -1) {
      I[i] = k;
      B[k] += S[i];
    }
  }
}

/*
 * Approximation algorithm: factor 2
 */
