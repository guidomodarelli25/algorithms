#include <vector>

using namespace std;

/* Next Fit */

/*
 * Se asignan objetos a cajones mientras sea posible. Los objetos se colocan
 * en un cajon mientras no superen la capacidad. Si esto sucede se habilita un
 * nuevo cajon. no se colocan objetos en los cajones ya considerados. Todo
 * objeto debe ser asignado a un cajon.
 */

void NextFit(vector<int>& S, vector<int>& B, vector<int>& I, int N) {
  for (int i = 0, j = 0; i < N; i++) {
    while (j < N) {
      if (B[j] + S[i] <= 10) {
        I[i] = j;
        B[j] += S[i];
        break;
      }
      j++;
    }
  }
}

/*
 * Approximation algorithm: factor 2
 * Time complexity: linear
 */
