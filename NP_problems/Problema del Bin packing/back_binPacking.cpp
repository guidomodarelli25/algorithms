#include <vector>

using namespace std;

void back_binPacking(vector<int>& S, vector<int>& B, vector<int>& I, int N,
                     int k, int cantBaldes, int& minCantBaldes,
                     vector<int>& best) {
  if (k == N) {
    if (minCantBaldes > cantBaldes) {
      minCantBaldes = cantBaldes;
      best = I;
    }
  } else {
    for (int i = 0; i < N; i++) {
      if (B[i] + S[k] <= 10) {
        I[k] = i;
        if (B[i] == 0) {
          cantBaldes++;
        }
        B[i] += S[k];
        back_binPacking(S, B, I, N, k + 1, cantBaldes, minCantBaldes, best);
        B[i] -= S[k];
        if (B[i] == 0) {
          cantBaldes--;
        }
        I[k] = -1;
      }
    }
  }
}
