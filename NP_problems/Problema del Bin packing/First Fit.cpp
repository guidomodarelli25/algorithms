#include <vector>

using namespace std;

/**
 * @brief First fit decreasing
 *
 * @details The objects are sorted in decreasing order by size. The objects are
 * placed in a bin while they do not exceed the capacity. If this happens a new
 * bin is enabled. The previous bins are considered.
 */

/* Sort S in decreasing order */

void FirstFit(vector<int>& S, vector<int>& B, vector<int>& I, int N) {
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      if (B[j] + S[i] <= 10) {
        I[i] = j;
        B[j] += S[i];
        break;
      }
    }
  }
}

/*
 * First Fit Decreasing
 * Approximation algorithm: factor 3/2
 * Time complexity: O(n^2)
 */

/*
 * First Fit
 * Approximation algorithm: factor 2
 * Time complexity: O(n^2)
 */
