#include <list>

#include "../../collections/Graph.cpp"

using namespace std;

/* Vertex Cover */

typedef pair<int, int> Arista;

void eliminar_incidentes(list<Arista>& aristas, int v1, int v2) {
  list<Arista>::iterator it = aristas.begin();
  while (it != aristas.end()) {
    int i1 = it->first, i2 = it->second;
    list<Arista>::iterator it2 = it++;
    if (v1 == i1 || v1 == i2 || v2 == i1 || v2 == i2) {
      aristas.erase(it2);
    }
  }
}

list<int> Vertex_Cover(const Graph& g) {
  list<int> out;
  list<Arista> aristas = g.getEdges();
  while (!aristas.empty()) {
    Arista a = aristas.front();
    eliminar_incidentes(aristas, a.first, a.second);
    out.push_back(a.first);
    out.push_back(a.second);
  }
  return out;
}

/*
 * Algoritmo de aproximacion: factor 2
 * Complejidad temporal: O(e)
 */
