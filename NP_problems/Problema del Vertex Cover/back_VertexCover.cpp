#include <vector>

#include "../../collections/Graph.cpp"

using namespace std;

typedef pair<int, int> Edge;

bool esSolucion(vector<bool>& solution, const Graph& g, int N) {
  list<Edge> edges = g.getEdges();
  foreach (it, edges) {
    bool encontre = false;
    for (int i = 0; i < N; i++) {
      if (solution[i]) {
        if (it->first == i || it->second == i) {
          encontre = true;
          break;
        }
      }
    }
    if (!encontre) {
      return false;
    }
  }
  return true;
}

void back_vertexCover(const Graph& g, int N, int k, int vertices,
                      int& minVertices, vector<bool>& solution,
                      vector<bool>& best) {
  if (k == N) {
    if (esSolucion(solution, g, N)) {
      if (minVertices > vertices) {
        minVertices = vertices;
        best = solution;
      }
    }
  } else {
    for (int i = 0; i <= 1; i++) {
      solution[k] = i;
      back_vertexCover(g, N, k + 1, vertices + i, minVertices, solution, best);
      solution[k] = 0;
    }
  }
}
