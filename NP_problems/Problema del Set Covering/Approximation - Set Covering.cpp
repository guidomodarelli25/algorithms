#include <algorithm>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

list<list<int> > Set_covering(vector<list<int> >& F) {
  list<list<int> > out;
  int n = F.size();
  vector<bool> U(n, true);
  /* Mientras no este vacio U */
  while (find(U.begin(), U.end(), true) != U.end()) {
    pair<int, int> max;
    for (int i = 0; i < n; i++) {
      int c = 0;
      foreach (j, F[i]) { c += U[*j]; }
      if (max.second < c) {
        max = make_pair(i, c);
      }
    }
    foreach (j, F[max.first]) { U[*j] = false; }
    out.push_back(F[max.first]);
    F[max.first].clear();
  }
  return out;
}

/*
 * Algoritmo de aproximacion: factor 11/6
 * Complejidad temporal: O(|X|.|F|.min(|X|,|F|))
 */
