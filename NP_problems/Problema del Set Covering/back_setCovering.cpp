#include <algorithm>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

bool esSolucion(vector<list<int> >& cerraduras, int c, int N) {
  vector<bool> used(c, 0);
  for (int i = 0; i < N; i++) {
    foreach (it, cerraduras[i]) { used[*it] = true; }
  }
  return find(used.begin(), used.end(), false) == used.end();
}

void back_setCovering(vector<list<int> >& F, vector<list<int> >& cerraduras,
                      int c, int N, int k, int nroLlave, int& minNroLlave,
                      vector<bool>& solution, vector<bool>& best) {
  if (k == N) {
    if (esSolucion(cerraduras, c, N)) {
      if (minNroLlave > nroLlave) {
        minNroLlave = nroLlave;
        best = solution;
      }
    }
  } else {
    for (int i = 0; i <= 1; i++) {
      solution[k] = i;
      if (i == 1) {
        cerraduras[k] = F[k];
      }
      back_setCovering(F, cerraduras, c, N, k + 1, nroLlave + i, minNroLlave,
                       solution, best);
      if (i == 1) {
        cerraduras[k].clear();
      }
      solution[k] = 0;
    }
  }
}
