#include <algorithm>
#include <climits>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

struct Node {
  vector<vector<int> > puzzle;
  int i, j;
  Node* sig;
};

typedef pair<Node*, int> Heap_node;

Node* duplicar(Node* n) {
  if (n != NULL) {
    Node* out = new Node;
    out->puzzle = n->puzzle;
    out->i = n->i;
    out->j = n->j;
    out->sig = duplicar(n->sig);
    return out;
  }
  return NULL;
}

void generarHijo(Node* n, vector<Node*>& hijos, int& nhijos, int k, int l) {
  nhijos++;
  Node* p = new Node;
  p->puzzle = n->puzzle;
  p->i = n->i + k;
  p->j = n->j + l;
  p->sig = duplicar(n);
  p->puzzle[n->i][n->j] = p->puzzle[n->i + k][n->j + l];
  p->puzzle[n->i + k][n->j + l] = 0;
  hijos[nhijos] = p;
}

int expandir(Node* n, vector<Node*>& hijos, int N) {
  int nhijos = 0;
  if (n->i < N) { /* abajo */
    generarHijo(n, hijos, nhijos, 1, 0);
  }
  if (n->j < N) { /* derecha */
    generarHijo(n, hijos, nhijos, 0, 1);
  }
  if (n->i > 1) { /* arriba */
    generarHijo(n, hijos, nhijos, -1, 0);
  }
  if (n->j > 1) { /* izquierda */
    generarHijo(n, hijos, nhijos, 0, -1);
  }
  return nhijos;
}

int h2(Node* n, int N) {
  int cuenta = 0, x, y;
  for (int i = 1; i <= N; i++) {
    for (int j = 1; j <= N; j++) {
      if (n->puzzle[i][j] == 0) {
        x = N;
        y = N;
      } else {
        x = ((n->puzzle[i][j] - 1) / N) + 1;
        y = ((n->puzzle[i][j] - 1) % N) + 1;
      }
      cuenta += abs(x - i) + abs(y - j);
    }
  }
  return cuenta;
}

int h(Node* n, int N) {
  int cuenta = 0;
  for (int i = 1; i <= N; i++) {
    for (int j = 1; j <= N; j++) {
      if (n->puzzle[i][j] != ((j + (i - 1) * N) % (N * N))) {
        cuenta++;
      }
    }
  }
  return cuenta;
}

bool esSolucion(Node* n, int N) { return h(n, N) == 0; }

bool sonIguales(vector<vector<int> >& M1, vector<vector<int> >& M2, int N) {
  for (int i = 1; i <= N; i++) {
    for (int j = 1; j <= N; j++) {
      if (M1[i][j] != M2[i][j]) {
        return false;
      }
    }
  }
  return true;
}

bool esAceptable(Node* n, int N) {
  Node* aux = n->sig;
  while (aux != NULL) {
    if (sonIguales(n->puzzle, aux->puzzle, N)) {
      return false;
    }
    aux = aux->sig;
  }
  return true;
}

Node* NodoInicial(int N) {
  Node* n = new Node;
  n->puzzle.resize(N + 1, vector<int>(N + 1, INT_MAX));
  n->puzzle[1][1] = 1;
  n->puzzle[1][2] = 5;
  n->puzzle[1][3] = 2;
  n->puzzle[2][1] = 4;
  n->puzzle[2][2] = 3;
  n->puzzle[2][3] = 0;
  n->puzzle[3][1] = 7;
  n->puzzle[3][2] = 8;
  n->puzzle[3][3] = 6;
  n->i = 2;
  n->j = 3;
  n->sig = NULL;
  return n;
}

void anadir(list<Heap_node>& E, Node* n, int key) {
  foreach (it, E) {
    if (it->second > key) {
      E.insert(it, make_pair(n, key));
      return;
    }
  }
  E.push_back(make_pair(n, key));
}

Node* ryp_puzzle(int N) {
  vector<Node*> hijos(5);
  list<Heap_node> E;
  Node* n = NodoInicial(N);
  anadir(E, n, h(n, N));
  while (!E.empty()) {
    n = E.front().first;
    E.pop_front();
    int numhijos = expandir(n, hijos, N);
    for (int i = 1; i <= numhijos; i++) {
      if (esAceptable(hijos[i], N)) {
        if (esSolucion(hijos[i], N)) {
          return hijos[i];
        } else {
          anadir(E, hijos[i], h(hijos[i], N));
        }
      }
    }
  }
  return NULL;
}
