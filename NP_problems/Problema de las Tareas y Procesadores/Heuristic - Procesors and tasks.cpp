#include <algorithm>
#include <vector>

using namespace std;

vector<int> assignment(vector<int>& tasks, int m) {
  sort(tasks.begin(), tasks.end());
  reverse(tasks.begin(), tasks.end());
  int n = tasks.size();
  vector<int> assignation(n, -1), procesors(m, 0);
  for (int i = 0; i < n && i < m; i++) {
    assignation[i] = i;
    procesors[i] = tasks[i];
  }
  if (n > m) {
    int maximum = tasks[0], j = m - 1;
    for (int i = m; i < n; i++) {
      while (j != -1 && procesors[j] + tasks[i] > maximum) {
        j--;
      }
      if (j == -1) {
        j = m - 1;
        for (int l = 0; l < m - 1; l++) {
          if (procesors[j] > procesors[l]) {
            j = l;
          }
        }
        maximum = procesors[j] + tasks[i];
      }
      procesors[j] += tasks[i];
      assignation[i] = j;
      j = m - 1;
    }
  }
  return assignation;
}
