#include <vector>

using namespace std;

void back_tareas(int N, int k, vector<vector<int> >& M, vector<bool>& used,
                 int accumulated_cost, int& best_cost, vector<int>& assignment,
                 vector<int>& best_assignment) {
  if (k == N) {
    if (best_cost > accumulated_cost) {
      best_cost = accumulated_cost;
      best_assignment = assignment;
    }
  } else {
    for (int i = 0; i < N; i++) {
      if (!used[i]) {
        assignment[k] = i;
        used[i] = true;
        back_tareas(N, k + 1, M, used, accumulated_cost + M[k][i], best_cost,
                    assignment, best_assignment);
        used[i] = false;
        assignment[k] = 0;
      }
    }
  }
}
