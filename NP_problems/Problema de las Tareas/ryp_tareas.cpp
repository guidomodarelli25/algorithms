#include <stddef.h>

#include <climits>
#include <list>
#include <utility>
#include <vector>

#include "../../defines.h"

using namespace std;

struct Node {
  vector<vector<int> > tarifas;
  int level;
  vector<int> solution;
};

typedef pair<Node*, int> Heap_node;

void copiar(Node* n1, Node* n2) {
  n1->tarifas = n2->tarifas;
  n1->level = n2->level;
  n1->solution = n2->solution;
}

bool noEsta(vector<int>& solution, int k, int j) {
  for (int i = 1; i <= k; i++) {
    if (solution[i] == j) {
      return false;
    }
  }
  return true;
}

void quitar(vector<vector<int> >& tarifas, int i, int j, int N) {
  int temp = tarifas[i][j];
  for (int k = 1; k <= N; k++) {
    tarifas[i][k] = INT_MAX;
    tarifas[k][j] = INT_MAX;
  }
  tarifas[i][j] = temp;
}

int expandir(Node* n, vector<Node*>& hijos, int N) {
  int nhijos = 0;
  int nk = n->level + 1;
  if (nk > N) {  // special case
    return nhijos;
  }
  for (int j = 1; j <= N; j++) {
    if (noEsta(n->solution, nk - 1, j)) {
      nhijos++;
      Node* p = new Node;
      copiar(p, n);
      p->solution[nk] = j;
      quitar(p->tarifas, nk, j, N);
      p->level++;
      hijos[nhijos] = p;
    }
  }
  return nhijos;
}

bool esSolucion(Node* n, int N) { return n->level == N; }

int columnCost(vector<vector<int> >& tarifas, int j, int N) {
  int c = tarifas[1][j];
  for (int i = 2; i <= N; i++) {
    if (c > tarifas[i][j]) {
      c = tarifas[i][j];
    }
  }
  return c;
}

int cost(vector<vector<int> >& tarifas, int N) {
  int sum = 0;
  for (int j = 1; j <= N; j++) {
    sum += columnCost(tarifas, j, N);
  }
  return sum;
}

int h(Node* n, int N) { return cost(n->tarifas, N); }

int Valor(Node* n, int N) { return h(n, N); }

bool esAceptable(Node* n, int N, int cota) { return Valor(n, N) <= cota; }

Node* NodoInicial(int N) {
  Node* n = new Node;
  n->solution.resize(N + 1, 0);
  n->level = 0;
  n->tarifas.resize(N + 1, vector<int>(N + 1, INT_MAX));
  /* cargar la matriz */
  return n;
}

void anadir(list<Heap_node>& E, Node* n, int key) {
  foreach (it, E) {
    if (it->second > key) {
      E.insert(it, make_pair(n, key));
      return;
    }
  }
  E.push_back(make_pair(n, key));
}

Node* ryp_tareas(int N) {
  vector<Node*> hijos(N + 1);
  int valor_actual, valor_solucion = INT_MAX, cota = valor_solucion;
  list<Heap_node> E;
  Node *n = NodoInicial(N), *solucion = NULL;
  anadir(E, n, h(n, N));
  while (!E.empty()) {
    n = E.front().first;
    E.pop_front();
    int numhijos = expandir(n, hijos, N);
    for (int i = 1; i <= numhijos; i++) {
      if (esAceptable(hijos[i], N, cota)) {
        if (esSolucion(hijos[i], N)) {
          valor_actual = Valor(hijos[i], N);
          if (valor_actual < valor_solucion) {
            solucion = hijos[i];
            valor_solucion = valor_actual;
            cota = valor_actual;
          }
        } else {
          anadir(E, hijos[i], h(hijos[i], N));
        }
      }
    }
  }
  return solucion;
}
