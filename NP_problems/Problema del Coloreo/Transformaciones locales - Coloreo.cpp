#include <vector>

#include "../../collections/Graph.cpp"

using namespace std;

void intercambiar(const Graph& g, int u, int v, vector<int>& colores) {
  // TODO: Implement
}

// The vector 'colores' is loaded with sequential coloring
void coloreo(Graph& g, vector<int>& colores) {
  for (int u = 0; u < g.size(); u++) {
    if (colores[u] > 2 && colores[u] < g.size()) {
      vector<int> adj = g.getAdjacentsVertices(u);
      for (unsigned i = 0; i < adj.size(); i++) {
        int v = adj[i];
        // Swap colors
        intercambiar(g, u, v, colores);
      }
    }
  }
}
