#include <algorithm>
#include <vector>

#include "../../collections/Graph.cpp"

using namespace std;

vector<int> assigned_color;

bool compare(int a, int b) { return assigned_color[a] < assigned_color[b]; }

vector<int> coloreo_secuencial(const Graph& g) {
  int n = g.size();
  assigned_color.resize(n, -1);
  assigned_color[0] = 0;
  for (int u = 1; u < n; u++) {
    int c = 0;
    vector<int> adj = g.getAdjacentsVertices(u);
    sort(adj.begin(), adj.end(), compare);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      if (assigned_color[v] == c) {
        c++;
      }
    }
    assigned_color[u] = c;
  }
  return assigned_color;
}
