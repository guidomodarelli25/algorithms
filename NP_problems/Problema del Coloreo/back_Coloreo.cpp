#include <vector>

#include "../../collections/Graph.cpp"

using namespace std;

bool poda(const Graph& g, vector<int>& solution, int N, int u, int color) {
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned v = 0; v < adj.size(); v++) {
    if (color == solution[adj[v]]) {
      return true;
    }
  }
  return false;
}

int cantidadColores(vector<int>& used, int N) {
  int cantColores = 0;
  for (int i = 0; i < N; i++) {
    if (used[i] != 0) {
      cantColores++;
    } else {
      break;
    }
  }
  return cantColores;
}

void back_coloreo(const Graph& g, int N, int k, int& minColores,
                  vector<int>& used, vector<int>& solution, vector<int>& best) {
  if (k == N) {
    int colores = cantidadColores(solution, N);
    if (minColores > colores) {
      minColores = colores;
      best = solution;
    }
  } else {
    for (int color = 0; color < minColores; color++) {
      if (!poda(g, solution, N, k, color)) {
        solution[k] = color;
        used[color]++;
        back_coloreo(g, N, k + 1, minColores, used, solution, best);
        used[color]--;
        solution[k] = -1;
      }
    }
  }
}
