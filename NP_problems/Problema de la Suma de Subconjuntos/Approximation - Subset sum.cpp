#include <algorithm>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

void remover(list<int>& L, int t) {
  list<int>::iterator it = L.begin(), it2;
  while (it != L.end()) {
    int Li = *it;
    it2 = it++;
    if (Li > t) {
      L.erase(it2);
    }
  }
}

void insertar_ordenado(list<int>& L, int xi) {
  foreach (it, L) {
    if (*it > xi) {
      L.insert(it, xi);
    }
  }
  L.push_back(xi);
}

list<int> merge_lists(list<int>& L, int xi) {
  list<int> L_out(L.begin(), L.end());
  foreach (it, L_out) { insertar_ordenado(L_out, *it + xi); }
  return L_out;
}

list<int> trim(list<int>& L, double d) {
  list<int> L_out;
  int yi = L.front();
  L_out.push_back(yi);
  int last = yi;
  for (list<int>::iterator it = ++L.begin(); it != L.end(); it++) {
    int yi = *it;
    if (yi > last * (1 + d)) {
      L_out.push_back(yi);
      last = yi;
    }
  }
  return L;
}

int Subset_sum(vector<int>& S, int t, double e) {
  int n = S.size();
  list<int> L;
  L.push_back(0);
  for (int i = 0; i < n; i++) {
    L = merge_lists(L, S[i]);
    L = trim(L, e / 2 * n);
    remover(L, t);
  }
  return *max_element(L.begin(), L.end());
}
