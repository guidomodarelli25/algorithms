#include <vector>

using namespace std;

void back_mochila(int N, int k, vector<int>& benefit, vector<int>& weight,
                  int accumulated_benefit, int accumulated_weight, int capacity,
                  int& best_benefit, vector<int>& solution,
                  vector<int>& best_solution) {
  if (k == N) {
    if (best_benefit < accumulated_benefit) {
      best_benefit = accumulated_benefit;
      best_solution = solution;
    }
  } else {
    for (int i = 0; i <= 1; i++) {
      if (weight[k] * i + accumulated_weight <= capacity) {
        solution[k] = i;
        back_mochila(N, k + 1, benefit, weight,
                     accumulated_benefit + benefit[k] * i,
                     accumulated_weight + weight[k] * i, capacity, best_benefit,
                     solution, best_solution);
        solution[k] = 0;
      }
    }
  }
}
