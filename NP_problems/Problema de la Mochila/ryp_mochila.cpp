#include <stddef.h>

#include <climits>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

struct Node {
  int accumulated_weight;
  int accumulated_benefit;
  int level;
  vector<int> solution;
};

typedef pair<Node*, int> Heap_node;

void copiar(Node* n1, Node* n2) {
  n1->accumulated_benefit = n2->accumulated_benefit;
  n1->accumulated_weight = n2->accumulated_weight;
  n1->level = n2->level;
  n1->solution = n2->solution;
}

int expandir(Node* n, vector<Node*>& hijos, int N, vector<int>& weight,
             vector<int>& benefit, int capacity) {
  int nhijos = 0;
  int i = n->level + 1;
  if (i > N) {  // special case
    return nhijos;
  }
  /* caso 0: we don't put him */
  nhijos++;
  Node* p = new Node;
  copiar(p, n);
  p->level++;  // no weight gain and benefits
  hijos[nhijos] = p;
  /* caso 1: we put him */
  if (n->accumulated_weight + weight[i] <= capacity) { /* has space */
    nhijos++;
    copiar(p, n);
    p->solution[i] = 1;
    p->level++;
    p->accumulated_weight += weight[i];
    p->accumulated_benefit += benefit[i];
    hijos[nhijos] = p;
  }
  return nhijos;
}

bool esSolucion(Node* n, int N) { return n->level == N; }

int h(Node* n, int N, vector<int>& weight, vector<int>& benefit, int capacity) {
  if (esSolucion(n, N)) {
    return n->accumulated_benefit;
  }
  int mejor =
      (float(benefit[n->level + 1]) / float(weight[n->level + 1])) + 0.5;
  return n->accumulated_benefit + (capacity - n->accumulated_weight) * mejor;
}

int Valor(Node* n, int N, vector<int>& weight, vector<int>& benefit,
          int capacity) {
  return INT_MAX - h(n, N, weight, benefit, capacity);
}

bool esAceptable(Node* n, int N, int cota, vector<int>& weight,
                 vector<int>& benefit, int capacity) {
  return Valor(n, N, weight, benefit, capacity) <= cota;
}

Node* NodoInicial(int N) {
  Node* n = new Node;
  n->solution.resize(N + 1, 0);
  n->accumulated_weight = 0;
  n->accumulated_benefit = 0;
  n->level = 0;
  return n;
}

void anadir(list<Heap_node>& E, Node* n, int key) {
  foreach (it, E) {
    if (it->second > key) {
      E.insert(it, make_pair(n, key));
      return;
    }
  }
  E.push_back(make_pair(n, key));
}

Node* ryp_mochila(int N, vector<int>& weight, vector<int>& benefit,
                  int capacity) {
  vector<Node*> hijos(3);
  int valor_actual, valor_solucion = INT_MAX, cota = valor_solucion;
  list<Heap_node> E;
  Node *n = NodoInicial(N), *solucion = NULL;
  anadir(E, n, h(n, N, weight, benefit, capacity));
  while (!E.empty()) {
    n = E.front().first;
    E.pop_front();
    int numhijos = expandir(n, hijos, N, weight, benefit, capacity);
    for (int i = 1; i <= numhijos; i++) {
      if (esAceptable(hijos[i], N, cota, weight, benefit, capacity)) {
        if (esSolucion(hijos[i], N)) {
          valor_actual = Valor(hijos[i], N, weight, benefit, capacity);
          if (valor_actual < valor_solucion) {
            solucion = hijos[i];
            valor_solucion = valor_actual;
            cota = valor_actual;
          }
        } else {
          anadir(E, hijos[i], h(hijos[i], N, weight, benefit, capacity));
        }
      }
    }
  }
  return solucion;
}
