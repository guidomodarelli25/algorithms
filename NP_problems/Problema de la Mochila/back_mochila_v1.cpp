#include <vector>

using namespace std;

void back_mochilaV1(vector<int>& P, int C, int accumulated_weight,
                    int& best_weight, int N, int k, vector<bool>& solution,
                    vector<bool>& best) {
  if (k == N) {
    if (best_weight < accumulated_weight) {
      best_weight = accumulated_weight;
      best = solution;
    }
  } else {
    for (int i = 0; i <= 1; i++) {
      if (P[k] * i + accumulated_weight <= C) {
        solution[k] = i;
        back_mochilaV1(P, C, accumulated_weight + P[k] * i, best_weight, N,
                       k + 1, solution, best);
        solution[k] = 0;
      }
    }
  }
}
