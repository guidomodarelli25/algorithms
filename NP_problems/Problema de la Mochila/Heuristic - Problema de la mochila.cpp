#include <algorithm>
#include <vector>

using namespace std;

vector<bool> mochila(vector<int>& objects, int C) {
  sort(objects.begin(), objects.end());
  reverse(objects.begin(), objects.end());
  int n = objects.size();
  vector<bool> assignation(n, false);
  int rest = 0;
  for (int i = 0; i < n; i++) {
    if (rest + objects[i] <= C) {
      assignation[i] = true;
      rest += objects[i];
    }
  }
  return assignation;
}
