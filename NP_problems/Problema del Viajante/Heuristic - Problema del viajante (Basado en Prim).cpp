#include <climits>
#include <list>

#include "../../collections/Graph.cpp"

using namespace std;

typedef pair<int, int> Arista;

Arista arch_minimum_cost(const Graph& g, list<int>& V, int c) {
  Arista a = make_pair(INT_MAX, INT_MAX);
  int costo = INT_MAX;
  foreach (v, V) {
    if (g.existsArch(c, *v) && g.getCost(c, *v) < costo) {
      a = make_pair(c, *v);
      costo = g.getCost(a);
    }
  }
  return a;
}

list<Arista> TSP_Prim(const Graph& g) {
  list<Arista> T;
  list<int> V = g.getVertices(), S;
  S.push_back(V.front());
  V.pop_front();
  while (!V.empty()) {
    int c = S.back();
    Arista a = arch_minimum_cost(g, V, c);
    T.push_back(a);
    S.push_back(a.second);
    V.remove(a.second);
  }
  T.push_back(make_pair(S.back(), S.front()));
  return T;
}

/*
 * Complejidad temporal: O(n^2)
 */
