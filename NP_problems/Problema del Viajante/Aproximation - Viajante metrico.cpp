#include <climits>
#include <list>

#include "../../collections/Graph.cpp"
#include "../../graphs/DFS.cpp"

using namespace std;

typedef pair<int, int> Edge;

Edge arch_minimum_cost(const Graph& g, list<int>& V, int c) {
  Edge a = make_pair(INT_MAX, INT_MAX);
  int costo = INT_MAX;
  foreach (v, V) {
    if (g.existsArch(c, *v) && g.getCost(c, *v) < costo) {
      a = make_pair(c, *v);
      costo = g.getCost(a);
    }
  }
  return a;
}

list<Edge> TSP_Prim(const Graph& g, int r) {
  list<Edge> T;
  list<int> V = g.getVertices(), S;
  S.push_back(r);
  V.remove(r);
  while (!V.empty()) {
    int c = S.back();
    Edge a = arch_minimum_cost(g, V, c);
    T.push_back(a);
    S.push_back(a.second);
    V.remove(a.second);
  }
  T.push_back(make_pair(S.back(), S.front()));
  return T;
}

list<Edge> TSP_Tour(const Graph& g, int c) {
  int r = g.getVertices().front();
  list<Edge> T = TSP_Prim(g, r);
  vector<int> H;
  dfs_forest(g);
  H = discovered;
}
