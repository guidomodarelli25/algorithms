#include <stddef.h>

#include <climits>
#include <list>
#include <vector>

#include "../../defines.h"

using namespace std;

struct Node {
  int accumulated_cost;
  int level;
  vector<vector<int> > reduced_matrix;
  vector<int> solution;
};

typedef pair<Node*, int> Heap_node;

void removeColumn(vector<vector<int> >& matriz, int j, int minimo, int N) {
  for (int i = 1; i <= N; i++) {
    if (matriz[i][j] < INT_MAX) {
      matriz[i][j] -= minimo;
    }
  }
}

void removeRow(vector<vector<int> >& matriz, int i, int minimo, int N) {
  for (int j = 1; j <= N; j++) {
    if (matriz[i][j] < INT_MAX) {
      matriz[i][j] -= minimo;
    }
  }
}

int columnCost(vector<vector<int> >& matriz, int j, int N) {
  int c = matriz[1][j];
  for (int i = 2; i <= N; i++) {
    c = min(c, matriz[i][j]);
  }
  return c;
}

int rowCost(vector<vector<int> >& matriz, int i, int N) {
  int c = matriz[i][1];
  for (int j = 2; j <= N; j++) {
    c = min(c, matriz[i][j]);
  }
  return c;
}

int reducir(vector<vector<int> >& matriz, int N) {
  int coste = 0;
  for (int i = 1; i <= N; i++) {
    int minimo = rowCost(matriz, i, N);
    if (minimo > 0 && minimo < INT_MAX) {
      removeRow(matriz, i, minimo, N);
      coste += minimo;
    }
  }
  for (int j = 1; j <= N; j++) {
    int minimo = columnCost(matriz, j, N);
    if (minimo > 0 && minimo < INT_MAX) {
      removeColumn(matriz, j, minimo, N);
      coste += minimo;
    }
  }
  return coste;
}

bool noEsta(const vector<int>& solution, int k, int j) {
  for (int i = 1; i <= k; i++) {
    if (solution[i] == j) {
      return false;
    }
  }
  return true;
}

void copiar(Node* n1, Node* n2) {
  n1->solution = n2->solution;
  n1->reduced_matrix = n2->reduced_matrix;
  n1->accumulated_cost = n2->accumulated_cost;
  n1->level = n2->level;
}

int expandir(Node* n, vector<Node*>& hijos, int N) {
  int nhijos = 0;
  int nk = n->level + 1;
  int i = n->solution[nk - 1];
  if (nk > N) {  // special case
    return nhijos;
  }
  for (int j = 1; j <= N; j++) {
    if (noEsta(n->solution, nk - 1, j)) {
      nhijos++;
      Node* p = new Node;
      copiar(p, n);
      p->solution[nk] = j;
      if (nk == N) {  // recorrido completo
        p->accumulated_cost +=
            n->reduced_matrix[i][j] + n->reduced_matrix[j][1];
      } else {
        for (int l = 1; l <= N; l++) {
          p->reduced_matrix[i][l] = INT_MAX;
          p->reduced_matrix[l][j] = INT_MAX;
        }
        p->reduced_matrix[j][1] = INT_MAX;
        p->accumulated_cost =
            reducir(p->reduced_matrix, N) + n->reduced_matrix[i][j];
      }
      p->level++;
      hijos[nhijos] = p;
    }
  }
  return nhijos;
}

int Valor(Node* n) { return n->accumulated_cost; }

bool esAceptable(Node* n, int cota) { return Valor(n) <= cota; }

int h(Node* n) { return n->accumulated_cost; }

bool esSolucion(Node* n, int N) { return n->level == N; }

Node* NodoInicial(int N) {
  Node* n = new Node;
  n->solution.resize(N + 1, 0);
  n->reduced_matrix.resize(N + 1, vector<int>(N + 1, INT_MAX));
  /* cargar la matriz */
  n->accumulated_cost = reducir(n->reduced_matrix, N);
  n->solution[1] = 1;
  n->level = 1;
  return n;
}

void anadir(list<Heap_node>& E, Node* n, int key) {
  foreach (it, E) {
    if (it->second > key) {
      E.insert(it, make_pair(n, key));
      return;
    }
  }
  E.push_back(make_pair(n, key));
}

Node* ryp_viajante(int N) {
  vector<Node*> hijos(N + 1);
  int valor_actual, valor_solucion = INT_MAX, cota = valor_solucion;
  list<Heap_node> E;
  Node *n = NodoInicial(N), *solucion = NULL;
  anadir(E, n, h(n));
  while (!E.empty()) {
    n = E.front().first;
    E.pop_front();
    int numhijos = expandir(n, hijos, N);
    for (int i = 1; i <= numhijos; i++) {
      if (esAceptable(hijos[i], cota)) {
        if (esSolucion(hijos[i], N)) {
          valor_actual = Valor(hijos[i]);
          if (valor_actual < valor_solucion) {
            solucion = hijos[i];
            valor_solucion = valor_actual;
            cota = valor_actual;
          }
        } else {
          anadir(E, hijos[i], h(hijos[i]));
        }
      }
    }
  }
  return solucion;
}
