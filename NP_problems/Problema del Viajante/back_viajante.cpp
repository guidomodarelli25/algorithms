#include <vector>

#include "../../collections/Graph.cpp"

using namespace std;

void back_viajante(const Graph& g, vector<bool>& visited, int u, int first,
                   int N, int k, vector<int>& way, int cost,
                   vector<int>& best_way, int& best_cost) {
  visited[u] = true;
  if (k == N) {
    if (best_cost > cost) {
      best_cost = cost;
      best_way = way;
    }
  } else {
    list<Graph::Arch> adjacents = g.getAdjacents(u);
    foreach (it, adjacents) {
      int v = it->getAdjacent();
      if (!visited[v] || (k + 1 == N && v == first)) {
        way[k] = v;
        back_viajante(g, visited, v, first, N, k + 1, way, cost + it->getCost(),
                      best_way, best_cost);
        if (v != first) {
          visited[v] = false;
          way[k] = -1;
        }
      }
    }
  }
}
