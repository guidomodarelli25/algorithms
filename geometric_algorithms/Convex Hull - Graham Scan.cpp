#include <algorithm>
#include <utility>
#include <vector>

#include "./defines.h"
#include "./main.cpp"

using namespace std;

/**
 * We sort the points by their y coordinate, then we sort them by their angle
 * with respect to the first point. Then we iterate over the points and check if
 * the angle formed by the points NEXT-TO-TOP(S), TOP(S) and Pi is turning to
 * the right. If it is, we pop the top point from the stack
 *
 * @param Q The set of points.
 *
 * @return The convex hull of the points.
 */
vector<Point> Graham_Scan(vector<Point>& Q) {
  /* Sorting the points by their y coordinate. */
  sort(Q.begin(), Q.end(), compareByY);
  /* Sorting the points by their angle with respect to the first point. */
  sort(Q.begin(), Q.end(), [&](const Point& p0, const Point& p1) {
    return crossProduct(p0, p1, Q[0]) < 0;
  });
  vector<Point> S;
  S.push_back(Q[0]);
  S.push_back(Q[1]);
  S.push_back(Q[2]);
  for (unsigned i = 3; i < Q.size(); i++) {
    /* Checking if the angle formed by the points NEXT-TO-TOP(S), TOP(S) and Pi
     * is turning to the right. */
    int n = S.size();
    while (n > 1 && crossProduct(S[n - 2], S[n - 1], Q[i]) > 0) {
      S.pop_back();
      n = S.size();
    }
    S.push_back(Q[i]);
  }
  return S;
}
