#include <float.h>
#include <limits.h>

#include <cmath>

#include "./defines.h"

using namespace std;

/**
 * It takes two points, and returns the distance between them
 *
 * @param p1 The first point.
 * @param p2 the second point.
 *
 * @return The distance between two points.
 */
float distance_(const Point& p1, const Point& p2) {
  return sqrt((p1.first - p2.first) * (p1.first - p2.first) +
              (p1.second - p2.second) * (p1.second - p2.second));
}

/**
 * For each pair of points, calculate the distance between them and return the
 * minimum distance
 *
 * @param P The array of points
 * @param n number of points
 *
 * @return The minimum distance between two points in the array.
 */
float bruteForce(Point P[], int n) {
  float min = FLT_MAX;
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n; j++) {
      if (min > distance_(P[i], P[j])) {
        min = distance_(P[i], P[j]);
      }
    }
  }
  return min;
}

/**
 * The function takes two pointers to points and returns the difference between
 * the second coordinate of the first point and the second coordinate of the
 * second point
 *
 * @param a Pointer to the first element to be compared.
 * @param b Pointer to the second element to be compared.
 *
 * @return The difference between the second coordinate of the first point and
 * the second coordinate of the second point.
 */
int compareY(const void* a, const void* b) {
  return ((Point*)a)->second < ((Point*)b)->second;
}

/**
 * We sort the points by their y-coordinate. Then, for each point, we check the
 * distance to all points that are within the minimum distance of the current
 * point
 *
 * @param P The array of points
 * @param n number of points
 * @param min The minimum distance between two points.
 *
 * @return The minimum distance between two points in the array.
 */
float stripClosest(Point P[], int n, float min) {
  qsort(P, n, sizeof(Point), compareY);
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n && min > (P[j].second - P[i].second); j++) {
      if (min > distance_(P[i], P[j])) {
        min = distance_(P[i], P[j]);
      }
    }
  }
  return min;
}

/**
 * If the number of points is greater than 3, then divide the points into two
 * halves, find the minimum distance in the left half, find the minimum distance
 * in the right half, and find the minimum distance between the points in the
 * two halves.
 *
 * @param P The array of points
 * @param n number of points
 *
 * @return The minimum distance between two points in the array.
 */
float closestUtil(Point P[], int n) {
  if (n > 3) {
    int mid = n / 2, j = 0;
    Point midPoint = P[mid], strip[n];
    float d = min(closestUtil(P, mid), closestUtil(P + mid, n - mid));
    for (int i = 0; i < n; i++) {
      if (abs(P[i].first - midPoint.first) < d) {
        strip[j++] = P[i];
      }
    }
    return min(d, stripClosest(strip, j, d));
  }
  return bruteForce(P, n);
}

/**
 * The function get the diffence between the first coordinate of the first point
 * and the first coordinate of the second point
 *
 * @param a Pointer to the first element to be compared.
 * @param b Pointer to the second element to be compared.
 *
 * @return The difference between the first coordinate of the first point and
 * the first coordinate of the second point.
 */
int compareX(const void* a, const void* b) {
  return ((Point*)a)->first - ((Point*)b)->first;
}

/**
 * The idea is to first sort the points by their x-coordinates. Then,
 * recursively find the closest distance between two points in the array.
 *
 * @param P Array of points
 * @param n number of points
 *
 * @return The minimum distance between any two points in the array.
 */
float closest(Point P[], int n) {
  qsort(P, n, sizeof(Point), compareX);
  return closestUtil(P, n);
}
