#include <algorithm>
#include <utility>
#include <vector>

using namespace std;

typedef pair<vector<int>, vector<int> > Segment;
typedef pair<vector<int>, Segment> Event;

vector<int> first(Segment s) {
  if (s.first[0] <= s.second[0]) {
    return s.first;
  } else {
    return s.second;
  }
}

vector<int> second(Segment s) {
  if (s.first[0] <= s.second[0]) {
    return s.second;
  } else {
    return s.first;
  }
}

struct Node {
  vector<int> point;
  Segment s;
  Node *left, *right;

  Node() : left(NULL), right(NULL) {}

  Node(vector<int> p, Segment s) : point(p), s(s), left(NULL), right(NULL) {}
};

Node* insert(Node* root, vector<int> point, Segment s, unsigned depth) {
  if (root == NULL) {
    return new Node(point, s);
  }
  unsigned axis = depth % 2;
  if (point[axis] < root->point[axis]) {
    root->left = insert(root->left, point, s, depth + 1);
  } else {
    root->right = insert(root->right, point, s, depth + 1);
  }
  return root;
}

Node* insert(Node* root, Segment s) { return insert(root, first(s), s, 0); }

Node* min(Node* a, Node* b, unsigned d) {
  Node* c = a;
  if (b != NULL) {
    if (c != NULL) {
      if (b->point[d] < c->point[d]) {
        c = b;
      }
    } else {
      c = b;
    }
  }
  return c;
}

Node* findMin(Node* root, unsigned d, unsigned depth) {
  if (root == NULL) {
    return NULL;
  }
  unsigned axis = depth % 2;
  if (axis == d) {
    if (root->left == NULL) {
      return root;
    }
    return findMin(root->left, d, depth + 1);
  }
  return min(root,
             min(findMin(root->right, d, depth + 1),
                 findMin(root->right, d, depth + 1), d),
             d);
}

bool isEquals(vector<int> p1, vector<int> p2) {
  for (int i = 0; i < 2; ++i) {
    if (p1[i] != p2[i]) {
      return false;
    }
  }
  return true;
}

bool isEquals(Node* root, vector<int> point, Segment s) {
  if (isEquals(root->point, point)) {
    if (second(root->s)[0] == second(s)[0] &&
        second(root->s)[1] == second(s)[1]) {
      return true;
    }
  }
  return false;
}

Node* Delete(Node* root, vector<int> point, Segment s, unsigned depth) {
  if (root == NULL) {
    return NULL;
  }
  int axis = depth % 2;
  if (isEquals(root, point, s)) {
    if (root->right != NULL) {
      Node* m = findMin(root->right, axis, depth + 1);
      root->point = m->point;
      root->s = m->s;
      root->right = Delete(root->right, m->point, s, depth + 1);
    } else if (root->left != NULL) {
      Node* m = findMin(root->left, axis, depth + 1);
      root->point = m->point;
      root->s = m->s;
      root->left = Delete(root->left, m->point, s, depth + 1);
    } else {
      delete root;
      return NULL;
    }
    return root;
  }
  if (point[axis] < root->point[axis]) {
    root->left = Delete(root->left, point, s, depth + 1);
  } else {
    root->right = Delete(root->right, point, s, depth + 1);
  }
  return root;
}

Node* Delete(Node* root, Segment s) { return Delete(root, first(s), s, 0); }

Node* Above(Node* root, Node* p, unsigned d, unsigned depth) {
  if (root == NULL) {
    return NULL;
  }
  unsigned axis = depth % 2;
  if (axis == 0) {
    if (root->point[axis] > p->point[axis]) {
      if (root->left == NULL) {
        return root;
      }
      return Above(root->left, p, d, depth + 1);
    }
    return Above(root->right, p, d, depth + 1);
  }
  if (root->point[axis] > p->point[axis]) {
    return min(root,
               min(Above(root->left, p, d, depth + 1),
                   Above(root->right, p, d, depth + 1), d),
               d);
  }
  return min(Above(root->left, p, d, depth + 1),
             Above(root->right, p, d, depth + 1), d);
}

Node* Above(Node* root, Segment s) {
  return Above(root, new Node(first(s), s), 0, 0);
}

bool areTheSameNodes(Node* b, Node* c) {
  if (b->point[0] == c->point[0] && b->point[1] == c->point[1]) {
    if (b->s.second[0] == c->s.second[0] && b->s.second[1] == c->s.second[1]) {
      return true;
    }
  }
  return false;
}

Node* maxOrEquals(Node* a, Node* b, unsigned d) {
  Node* c = a;
  if (b != NULL) {
    if (c != NULL) {
      if (b->point[d] > c->point[d]) {
        c = b;
      } else if (b->point[d] == c->point[d] && !areTheSameNodes(b, c)) {
        c = b;
      }
    } else {
      c = b;
    }
  }
  return c;
}

Node* Below(Node* root, Node* p, unsigned d, unsigned depth) {
  if (root == NULL) {
    return NULL;
  }
  unsigned axis = depth % 2;
  if (axis == 0) {
    if (root->point[axis] < p->point[axis] ||
        (root->point[axis] == p->point[axis] && !areTheSameNodes(root, p))) {
      if (root->right == NULL) {
        return root;
      }
      return maxOrEquals(root, Below(root->right, p, d, depth + 1), d);
    }
    return Below(root->left, p, d, depth + 1);
  }
  if (root->point[axis] < p->point[axis] ||
      (root->point[axis] == p->point[axis] && !areTheSameNodes(root, p))) {
    return maxOrEquals(root,
                       maxOrEquals(Below(root->left, p, d, depth + 1),
                                   Below(root->right, p, d, depth + 1), d),
                       d);
  }
  return maxOrEquals(Below(root->left, p, d, depth + 1),
                     Below(root->right, p, d, depth + 1), d);
}

Node* Below(Node* root, Segment s) {
  return Below(root, new Node(first(s), s), 0, 0);
}

int crossProduct(vector<int> pi, vector<int> pj, vector<int> pk) {
  return (pk[0] - pi[0]) * (pj[1] - pi[1]) - (pk[1] - pi[1]) * (pj[0] - pi[0]);
}

bool onSegment(vector<int> pi, vector<int> pj, vector<int> pk) {
  if (min(pi[0], pj[0]) <= pk[0] && pk[0] <= max(pi[0], pj[0]) &&
      min(pi[1], pj[1]) <= pk[1] && pk[1] <= max(pi[1], pj[1])) {
    return true;
  }
  return false;
}

bool intersect(vector<int> p1, vector<int> p2, vector<int> p3, vector<int> p4) {
  int d1 = crossProduct(p3, p4, p1);
  int d2 = crossProduct(p3, p4, p2);
  int d3 = crossProduct(p1, p2, p3);
  int d4 = crossProduct(p1, p2, p4);
  if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
      ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) {
    return true;
  } else if (d1 == 0 && onSegment(p3, p4, p1)) {
    return true;
  } else if (d2 == 0 && onSegment(p3, p4, p2)) {
    return true;
  } else if (d3 == 0 && onSegment(p1, p2, p3)) {
    return true;
  } else if (d4 == 0 && onSegment(p1, p2, p4)) {
    return true;
  } else {
    return false;
  }
}

bool intersect(const Segment& a, const Segment& b) {
  return intersect(a.first, a.second, b.first, b.second);
}

bool leftmost(vector<int> a, Segment s) {
  return a[0] == first(s)[0] && a[1] == first(s)[1];
}

bool rightmost(vector<int> a, Segment s) {
  return a[0] == second(s)[0] && a[1] == second(s)[1];
}

bool comp(Event a, Event b) {
  return a.first[0] < b.first[0] ||
         (a.first[0] == b.first[0] && a.first[1] < b.first[1]);
}

/**
 * It sorts the segments by their leftmost point, and then it inserts the
 * segments into a tree. Then it checks if the segment above and below
 * intersect. If they do, then it returns true. If not, then it continues
 *
 * @param Q A vector of segments.
 *
 * @return true if there is an intersection, false otherwise.
 */
bool anySegmentsIntersect(vector<Segment>& Q) {
  Node* T = NULL;
  int N = Q.size();
  vector<Event> extremes(2 * N);
  for (int i = 0; i < N; i++) {
    extremes[i] = make_pair(Q[i].first, Q[i]);
  }
  sort(extremes.begin(), extremes.begin() + N, comp);
  for (int i = 0; i < N; i++) {
    extremes[i + N] = make_pair(Q[i].second, Q[i]);
  }
  sort(extremes.begin() + N, extremes.end(), comp);
  for (int i = 0; i < 2 * N; i++) {
    vector<int> p = extremes[i].first;
    Segment s = extremes[i].second;
    /* This is checking if the segment is the leftmost segment of the event. If
    it is, then it inserts the segment into the tree. Then it checks if the
    segment above and below intersect. If they do, then it returns true. If not,
    then it continues. */
    if (leftmost(p, s)) {
      T = insert(T, s);
      Node *it_above = Above(T, s), *it_below = Below(T, s);
      if ((it_above != NULL && intersect(it_above->s, s)) ||
          (it_below != NULL && intersect(it_below->s, s))) {
        return true;
      }
    }
    /* This is checking if the segment is the rightmost segment of the event. If
    it is, then it checks if the segment above
    and below intersect. If they do, then it returns true. If not, then it
    deletes the segment from the tree. */
    if (rightmost(p, s)) {
      Node *it_above = Above(T, s), *it_below = Below(T, s);
      if ((it_above != NULL && it_below != NULL) &&
          intersect(it_above->s, it_below->s)) {
        return true;
      }
      T = Delete(T, s);
    }
  }
  return false;
}
