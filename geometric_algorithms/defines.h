#ifndef GEO_DEFINES
#define GEO_DEFINES

#include <utility>

using namespace std;

typedef pair<int, int> Point;

typedef pair<Point, Point> Segment;

#endif  // GEO_DEFINES
