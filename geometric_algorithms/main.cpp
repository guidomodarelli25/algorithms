#include <climits>
#include <list>
#include <utility>

#include "../defines.h"
#include "./defines.h"
using namespace std;

/**
 * The cross product of two vectors.
 *
 * @param v1 The first vector.
 * @param v2 The second vector.
 *
 * @return The cross product of two vectors.
 */
double crossProduct(const Point& v1, const Point& v2) {
  return v1.first * v2.second - v2.first * v1.second;
}

/**
 * The cross product between two vectors, p0p1 and p0p2.
 *
 * @param p0 The first point.
 * @param p1 The second point.
 * @param p2 the third point.
 *
 * @return The cross product of the vectors p0p1 and p0p2.
 */
float crossProduct(const Point& p0, const Point& p1, const Point& p2) {
  return ((p2.first - p0.first) * (p1.second - p0.second) -
          (p2.second - p0.second) * (p1.first - p0.first));
}

/**
 * compare two points, if the first one is less or equals than the second one.
 *
 * @param a The first point to compare.
 * @param b The second point to compare.
 *
 * @return true if a < b, false otherwise.
 */
bool compare(const Point& a, const Point& b) {
  return a.first < b.first || (a.first == b.first && a.second < b.second);
}

/**
 * Compares two points, if the first one is less or equals than the second one.
 *
 * @param p1 The first point to compare.
 * @param p2 The point to compare against.
 *
 * @return true if the first point is less than the second point, false otherwise.
 */
bool compareByY(const Point& p1, const Point& p2) {
  return (p1.second < p2.second) ||
         (p1.second == p2.second && p1.first < p2.first);
}

/**
 * Verify if a Point is left or right of a Segment
 *
 * @param S The segment we're checking against.
 * @param P The point we're testing.
 *
 * @return negative if P is left of S, positive if P is right of S, otherwise 0.
 */
int left_right(const Segment& S, const Point& P) {
  return crossProduct(
      make_pair(P.first - S.first.first, P.second - S.first.second),
      make_pair(S.second.first - S.first.first,
                S.second.second - S.first.second));
}

/**
 * If the two segments have a common point, then they intersect
 *
 * @param S The first segment
 * @param Q The query segment
 *
 * @return true if the two segments intersect, false otherwise
 */
bool has_cut(const Segment& S, const Segment& Q) {
  return max(S.first.first, S.second.first) >=
             min(Q.first.first, Q.second.first) &&
         max(Q.first.first, Q.second.first) >=
             min(S.first.first, S.second.first) &&
         max(S.first.second, S.second.second) >=
             min(Q.first.second, Q.second.second) &&
         max(Q.first.second, Q.second.second) >=
             min(S.first.second, S.second.second);
}

/**
 * Verify if both Points of a Segment are on the same side of a Segment
 *
 * @param S The segment we're checking against.
 * @param Q The segment we're testing.
 *
 * @return true if both Points of a Segment are on the same side of a Segment,
 * otherwise false.
 */
bool same_side(const Segment& S, const Segment& Q) {
  return left_right(S, Q.first) * left_right(S, Q.second) <= 0 &&
         left_right(Q, S.first) * left_right(Q, S.second) <= 0;
}

/**
 * If the point is on the left of all the segments, then it's inside the polygon
 *
 * @param polygon A list of segments that form a polygon.
 * @param P The point you want to check.
 *
 * @return true if the point is inside the polygon, false otherwise.
 */
bool inside_polygon(const list<Segment>& polygon, const Point& P) {
  foreach (it, polygon) {
    Segment seg = *it;
    if (left_right(seg, P) >= 0) {
      return false;
    }
  }
  return true;
}

/**
 * If the number of times the segment from the point to infinity cuts the
 * polygon is odd, then the point is inside the polygon
 *
 * @param polygon A list of segments that form the polygon.
 * @param P The point to test.
 *
 * @return true if the point is inside the polygon, false otherwise.
 */
bool lightning_test(const list<Segment>& polygon, const Point& P) {
  int cuts = 0;
  Segment S = make_pair(P, make_pair(INT_MAX, P.second));
  foreach (it, polygon) {
    Segment seg = *it;
    if (max(seg.first.first, seg.second.first) > S.first.first &&
        same_side(seg, S)) {
      cuts++;
    }
  }
  return cuts % 2 != 0;
}

/**
 * The area of a polygon is the sum of the areas of the triangles formed by the
 * polygon's vertices and the origin.
 *
 * @param polygon a list of segments that form a polygon.
 *
 * @return The area of the polygon.
 */
double polygon_area(const list<Segment>& polygon) {
  double area = 0;
  foreach (it, polygon) {
    Segment seg = *it;
    area += crossProduct(seg.first, seg.second);
  }
  return area;
}
