#include <algorithm>
#include <set>
#include <utility>
#include <vector>

#include "./defines.h"

using namespace std;

/**
 * If the point is in the first quadrant, return 1. If the point is in the
 * second quadrant, return 2. If the point is in the third quadrant, return 3.
 * Otherwise, return 4
 *
 * @param p The point to check.
 *
 * @return The quadrant of the point.
 */
int quadrant(const Point& p) {
  return p.first >= 0 && p.second >= 0   ? 1
         : p.first <= 0 && p.second >= 0 ? 2
         : p.first <= 0 && p.second <= 0 ? 3
                                         : 4;
}

/**
 * It returns the dot product of two points
 *
 * @param a the first point
 * @param b the second point
 *
 * @return The product of the two points.
 */
int productPoint(const Point& a, const Point& b) {
  return a.first * b.first + a.second * b.second;
}

int crossProduct(const Point& a, const Point& b) {
  return a.first * b.second - a.second * b.first;
}

int crossProduct(const Point& a, const Point& b, const Point& c) {
  return (b.second - a.second) * (c.first - b.first) -
         (c.second - b.second) * (b.first - a.first);
}

/**
 * It finds the tangent between two convex polygons
 *
 * @param a the first polygon
 * @param b the polygon we're trying to find the tangent to
 * @param i_a index of the point in the first polygon
 * @param i_b the index of the point in the second polygon
 *
 * @return The tangent between the two polygons.
 */
Point find_tangent(const vector<Point>& a, const vector<Point>& b, int i_a,
                   int i_b) {
  int n = a.size(), m = b.size();
  int i_a2 = (i_a + 1) % n;
  int i_b2 = (m + i_b - 1) % m;
  while (crossProduct(b[i_b], a[i_a], a[i_a2]) >= 0) {
    i_a = (i_a + 1) % n;
    i_a2 = (i_a + 1) % n;
  }
  while (crossProduct(a[i_a], b[i_b], b[i_b2]) <= 0) {
    i_b = (m + i_b - 1) % m;
    i_b2 = (m + i_b - 1) % m;
  }
  return make_pair(i_a, i_b);
}

/**
 * It returns the index of the point in the vector that is farthest from the
 * origin along the line with slope k
 *
 * @param a the array of points
 * @param k the slope of the line
 *
 * @return The index of the point in the vector that is the farthest from the
 * origin.
 */
int farthest_point(const vector<Point>& a, int k) {
  int j = 0, n = a.size();
  for (int i = 1; i < n; i++) {
    if (a[i].first * k > a[j].first * k) {
      j = i;
    }
  }
  return j;
}

/**
 * It takes a vector of points, and two indices into that vector, and returns a
 * vector of points that is the convex hull of the points between the two
 * indices
 *
 * @param a the vector of points
 * @param i index of the first point in the first convex hull
 * @param i_2 the index of the point in the second convex hull that is the first
 * point of the second convex hull.
 *
 * @return The convex hull of the points in the vector a.
 */
vector<Point> merge_convex_hulls(const vector<Point>& a, int i, int i_2) {
  vector<Point> out(1, a[i]);
  int n = a.size();
  while (i != i_2) {
    i = (i + 1) % n;
    out.push_back(a[i]);
  }
  return out;
}

/**
 * "Find the farthest points in each convex hull, find the tangents between
 * them, and merge the two convex hulls."
 *
 * The first step is to find the farthest points in each convex hull. This is
 * done by finding the point with the largest dot product with the vector
 * between the two convex hulls
 *
 * @param a The first convex hull
 * @param b the second convex hull
 *
 * @return The convex hull of the union of the two convex hulls.
 */
vector<Point> merger(const vector<Point>& a, const vector<Point>& b) {
  int i_a = farthest_point(a, 1);
  int i_b = farthest_point(b, -1);
  Point upper = find_tangent(a, b, i_a, i_b);
  Point lower = find_tangent(b, a, i_b, i_a);
  vector<Point> out = merge_convex_hulls(a, upper.first, lower.second);
  vector<Point> out_2 = merge_convex_hulls(b, lower.first, upper.second);
  out.insert(out.end(), out_2.begin(), out_2.end());
  return out;
}

/**
 * It takes a vector of points and returns a set of points that are on the edge
 * of the convex hull
 *
 * @param a vector of points
 *
 * @return A set of points that are on the edge of the polygon.
 */
set<Point> points_on_edge(vector<Point>& a) {
  set<Point> s;
  int n = a.size();
  for (int i = 0; i < n - 1; i++) {
    for (int j = i + 1, temp = 0, temp2 = 0; j < n; j++) {
      for (int k = 0; k < n; k++) {
        Point c = make_pair(a[k].second, a[k].first);
        int valor = productPoint(a[i], c) - productPoint(a[j], c) +
                    crossProduct(a[i], a[j]);
        valor == 0 ? temp2++, temp++ : valor < 0 ? temp2++ : temp++;
      }
      if (temp == n || temp2 == n) {
        s.insert(a[i]);
        s.insert(a[j]);
      }
    }
  }
  return s;
}

Point mid;

bool compare(const Point& a, const Point& b) {
  Point p = make_pair(a.first - mid.first, a.second - mid.second);
  Point q = make_pair(b.first - mid.first, b.second - mid.second);
  int one = quadrant(p), two = quadrant(q);
  return one != two ? one < two : p.second * q.first < q.second * p.first;
}

vector<Point> bruteHull(vector<Point>& a) {
  set<Point> s = points_on_edge(a);
  vector<Point> out(s.begin(), s.end());
  int n = out.size();
  mid = make_pair(0, 0);
  for (int i = 0; i < n; i++) {
    mid = make_pair(mid.first + out[i].first, mid.second + out[i].second);
    out[i] = make_pair(out[i].first * n, out[i].second * n);
  }
  sort(out.begin(), out.end(), compare);
  for (int i = 0; i < n; i++) {
    out[i] = make_pair(out[i].first / n, out[i].second / n);
  }
  return out;
}

vector<Point> divide_and_conquer(vector<Point>& a) {
  int n = a.size();
  if (n >= 6) {
    vector<Point> left, right;
    for (int i = 0; i < n / 2; i++) {
      left.push_back(a[i]);
    }
    for (int i = n / 2; i < n; i++) {
      right.push_back(a[i]);
    }
    return merger(divide_and_conquer(left), divide_and_conquer(right));
  }
  return bruteHull(a);
}

vector<Point> ConvexHull(vector<Point>& a) {
  sort(a.begin(), a.end());
  return divide_and_conquer(a);
}
