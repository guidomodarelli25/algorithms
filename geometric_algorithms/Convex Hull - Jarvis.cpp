#include <algorithm>
#include <utility>
#include <vector>

#include "./defines.h"
#include "./main.cpp"

using namespace std;

/**
 * The function takes a set of points and returns the points on the convex hull
 * in counterclockwise order
 *
 * @param S The set of points
 *
 * @return A vector of points that make up the convex hull.
 */
vector<Point> Jarvis(const vector<Point>& S) {
  Point pointOnHull = *min_element(S.begin(), S.end(), compare), endpoint;
  vector<Point> P;
  do {
    P.push_back(pointOnHull), endpoint = S[0];
    for (unsigned j = 1; j < S.size(); j++) {
      if ((endpoint.first == pointOnHull.first &&
           endpoint.second == pointOnHull.second) ||
          (crossProduct(P.back(), endpoint, S[j]) < 0)) {
        endpoint = S[j];
      }
    }
    pointOnHull = endpoint;
  } while ((endpoint.first != P[0].first) || (endpoint.second != P[0].second));
  return P;
}
