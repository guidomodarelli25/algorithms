#ifndef SET_H_INCLUDED
#define SET_H_INCLUDED

#include <vector>

class Set {
 private:
  int* p;
  int N;

 public:
  /**
   * It creates a set of size n.
   *
   * @param n the number of elements in the set
   */
  Set(int n) {
    this->N = n;
    p = new int[N];
    for (int i = 0; i < N; i++) {
      p[i] = -1;
    }
  }

  /**
   * union between two sets
   *
   * @param i the first set
   * @param j the second set
   */
  void union_(int i, int j) {
    int temp = p[i] + p[j];
    if (p[i] > p[j]) {
      p[i] = j;
      p[j] = temp;
    } else {
      p[j] = i;
      p[i] = temp;
    }
    N--;
  }

  /**
   * The function finds the root of the tree containing the element i
   *
   * @param i the index of the element you want to find the root of
   *
   * @return The root of the tree.
   */
  int find(int i) {
    int r = i;
    while (p[r] >= 0) {
      r = p[r]; /* found it the root */
    }
    while (i != r) {
      int temp = p[i];
      p[i] = r;
      i = temp;
    }
    return r;
  }
};

#endif  // SET_H_INCLUDED
