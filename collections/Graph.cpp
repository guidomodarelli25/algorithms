#ifndef GRAFO_H_INCLUDED
#define GRAFO_H_INCLUDED

#include <cassert>
#include <list>
#include <map>
#include <set>
#include <vector>

#include "../defines.h"

using namespace std;

typedef pair<int, int> Edge;

class Graph {
 private:
  map<int, map<int, int> > graph;

  /**
   * It finds the first element in the list that is greater than the given
   * element. If there is no such element, it returns the last element.
   *
   * @param L The list to search
   * @param a The edge to search
   *
   * @return The iterator to the first element in the list that is greater than
   * the edge being passed in. If there is no such element, it returns the last
   */
  list<Edge>::iterator find(list<Edge>& L, const Edge& a) const {
    foreach (it, L) {
      if (getCost(*it) > getCost(a)) {
        return it;
      }
    }
    return L.end();
  }

 public:
  class Arch {
   private:
    int adjacent;
    int cost;

   public:
    /**
     * The function takes two integers as parameters and assigns them to the
     * adjacent and cost variables
     *
     * @param adjacent The index of the node that this arch is connected to.
     * @param cost the cost of the arch
     */
    Arch(int adjacent, int cost) {
      this->adjacent = adjacent;
      this->cost = cost;
    }

    /**
     * Returns the number of adjacent vertex.
     *
     * @return The value of the adjacent variable.
     */
    int getAdjacent() const { return adjacent; }

    /**
     * returns the cost of the arc.
     *
     * @return The cost of the arc.
     */
    int getCost() const { return cost; }
  };  // class Arch

  Graph() {}

  /**
   * It creates a graph with n vertices.
   *
   * @param n The number of vertices to add to the graph.
   */
  Graph(int n) {
    while (--n > -1) {
      addVertex(n);
    }
  }

  /**
   * Copy the reference graph g into this graph
   *
   * @param g The graph to copy.
   */
  Graph(const Graph& g) { *this = g; }

  /**
   * The function takes a list of vertices and adds them to the graph
   *
   * @param vertices a list of vertices to add to the graph
   */
  Graph(list<int>& vertices) {
    foreach (u, vertices) { addVertex(*u); }
  }

  /**
   * It copies the graph g into the current graph
   *
   * @return A reference to the object that called the function.
   */
  Graph& operator=(const Graph& g) {
    list<int> vertices = g.getVertices();
    foreach (u, vertices) {
      addVertex(*u);
      list<Arch> edges = g.getAdjacents(*u);
      foreach (v, edges) {
        addVertex(v->getAdjacent());
        addArch(*u, v->getAdjacent(), v->getCost());
      }
    }
    return *this;
  }

  /**
   * It creates a new graph, and for each vertex in the original graph, it adds
   * a vertex to the new graph, and for each edge in the original graph, it adds
   * an edge to the new graph, but with the vertices reversed
   *
   * @return A reference to the object that called the function.
   */
  Graph& reverse(const Graph& g) {
    list<int> vertices = g.getVertices();
    foreach (u, vertices) {
      addVertex(*u);
      list<Arch> adjs = g.getAdjacents(*u);
      foreach (v, adjs) {
        addVertex(v->getAdjacent());
        addArch(v->getAdjacent(), *u, v->getCost());
      }
    }
    return *this;
  }

  /**
   * If the vertex doesn't exist, add it to the graph
   *
   * @param u The vertex to add to the graph
   */
  void addVertex(int u) {
    if (!existsVertex(u)) {
      map<int, int> edges;
      graph[u] = edges;
    }
  }

  // PRE-CONDITION: existsVertex(u) && existsVertex(v)
  // POST-CONDITION: existsArch(u, v)
  /**
   * adds an arch from vertex u to vertex v with cost cost
   *
   * @param u the first vertex
   * @param v the vertex to add
   * @param cost the cost of the arch
   */
  void addArch(int u, int v, int cost) {
    if (existsVertex(u) && existsVertex(v)) {
      graph.find(u)->second[v] = cost;
    }
  }

  /**
   * adds an edge between u and v with weight cost
   *
   * @param u The first vertex of the edge.
   * @param v The second vertex of the edge.
   * @param cost the cost of the edge
   */
  void addEdge(int u, int v, int cost) {
    addArch(u, v, cost);
    addArch(v, u, cost);
  }

  /**
   * It adds an arc from node u to node v, with a cost of 0
   *
   * @param u the first node
   * @param v the second node
   */
  void addArch(int u, int v) { addArch(u, v, 0); }

  /**
   * It adds an edge from vertex u to vertex v with weight 0
   *
   * @param u The first vertex of the edge.
   * @param v The second vertex of the edge.
   */
  void addEdge(int u, int v) { addEdge(u, v, 0); }

  // POST-CONDITION: For all u v != u: !existsArch(v, u) && !existsArch(u, v)
  /**
   * removes a vertex from the graph
   *
   * @param u The vertex to be erased.
   */
  void eraseVertex(int u) {
    graph.erase(u);
    foreach (it, graph) { it->second.erase(u); }
  }

  // POST-CONDITION: !existsArch(u, v)
  /**
   * It removes the arch from the graph
   *
   * @param u the first vertex of the arch
   * @param v the vertex to be erased
   */
  void eraseArch(int u, int v) {
    iterator(graph) it = graph.find(u);
    if (it != graph.end()) {
      it->second.erase(v);
    }
  }

  /**
   * It clears the graph
   */
  void clear() { graph.clear(); }

  /**
   * Returns true if the graph is empty, false otherwise.
   *
   * @return The graph is being returned.
   */
  bool empty() const { return graph.empty(); }

  /**
   * Returns the number of vertices in the graph.
   *
   * @return The size of the graph.
   */
  unsigned size() const { return graph.size(); }

  /**
   * If the graph contains a vertex with the given label, return true, otherwise
   * return false.
   *
   * @param u The vertex to check for.
   *
   * @return true if the graph contains the vertex, false otherwise.
   */
  bool existsVertex(int u) const { return graph.find(u) != graph.end(); }

  /**
   * If the graph contains the node u, and the node u contains the node v, then
   * return true
   *
   * @param u the first vertex of the arch
   * @param v the second vertex of the arch
   *
   * @return true if the graph contains the arch, false otherwise.
   */
  bool existsArch(int u, int v) const {
    iterator(graph) it = graph.find(u);
    return it != graph.end() && it->second.find(v) != it->second.end();
  }

  /**
   * Returns true if there is an arc from node a.first to node a.second
   *
   * @param a the edge to check
   *
   * @return true if the graph contains the edge, false otherwise.
   */
  bool existsArch(const Edge& a) const { return existsArch(a.first, a.second); }

  /**
   * It returns a list of vertices in the graph.
   *
   * @return A list of vertices.
   */
  list<int> getVertices() const {
    list<int> vertices;
    foreach (it, graph) { vertices.push_front(it->first); }
    return vertices;
  }

  /**
   * It returns a list of Arch objects, which are the adjacent vertices of the
   * vertex u
   *
   * @param u The node you want to get the adjacents from.
   *
   * @return A list of Archs
   */
  list<Arch> getAdjacents(int u) const {
    list<Arch> adyacentes;
    iterator(graph) it = graph.find(u);
    if (it != graph.end()) {
      foreach (it2, it->second) {
        adyacentes.push_front(Arch(it2->first, it2->second));
      }
    }
    return adyacentes;
  }

  /**
   * It returns a vector of all the vertices adjacent to the vertex u
   *
   * @param u the vertex you want to get the adjacent vertices of
   *
   * @return A vector of integers.
   */
  vector<int> getAdjacentsVertices(int u) const {
    vector<int> out;
    list<Arch> A = getAdjacents(u);
    foreach (v, A) { out.push_back(v->getAdjacent()); }
    return out;
  }

  /**
   * For each vertex in the graph, add an edge to the list of edges for each of
   * its neighbors
   *
   * @return A list of edges.
   */
  list<Edge> getEdges() const {
    list<Edge> edges;
    foreach (u, graph) {
      foreach (v, u->second) { edges.push_back(make_pair(u->first, v->first)); }
    }
    return edges;
  }

  /**
   * Given a list of edges, return a list of edges ordered
   *
   * @return A list of edges.
   */
  list<Edge> getOrderedEdges() const {
    list<Edge> ans;
    list<Edge> edges = getEdges();
    foreach (it, edges) { ans.insert(find(ans, *it), *it); }
    return ans;
  }

  /**
   * It returns a set of edges, where the edges are the edges of the graph
   *
   * @return A set of edges.
   */
  set<Edge> getSetEdges() const {
    list<Edge> Edges = getEdges();
    return set<Edge>(Edges.begin(), Edges.end());
  }

  // PRE-CONDITION: existsArch(u, v)
  /**
   * If the arch exists, return the cost of the arch
   *
   * @param u The first vertex of the edge.
   * @param v The vertex to be added to the graph.
   *
   * @return The cost of the arch between u and v.
   */
  int getCost(int u, int v) const {
    assert(existsArch(u, v));
    return graph.find(u)->second.find(v)->second;
  }

  /**
   * Return the cost of the edge between the two vertices.
   *
   * @param a The edge to get the cost of.
   *
   * @return The cost of the edge.
   */
  int getCost(const Edge& a) const { return getCost(a.first, a.second); }

  /**
   * For each vertex in the graph, if the vertex is adjacent to the input
   * vertex, add the edge to the output list
   *
   * @param v the vertex to get the incident edges of
   *
   * @return A list of edges that are incident to vertex v.
   */
  list<Edge> getIncidents(int v) const {
    list<Edge> ans;
    foreach (u, graph) {
      if (u->second.find(v) != u->second.end()) {
        ans.push_back(make_pair(u->first, v));
      }
    }
    return ans;
  }

};  // class Graph

#endif  // GRAFO_H_INCLUDED
