#ifndef HEAP_H_INCLUDED
#define HEAP_H_INCLUDED

#include <climits>
#include <vector>

using namespace std;

class Heap {
 private:
  typedef pair<int, int> Node;

  int n;

  vector<int> pos;
  vector<Node> array;

  /**
   * It initializes the data structure.
   *
   * @param n the number of elements in the heap
   */
  void init(int n) {
    this->n = n;
    pos.resize(n);
    array.resize(n);
    for (int i = 0; i < n; i++) {
      pos[i] = i;
      array[i] = make_pair(i, INT_MAX);
    }
  }

  /**
   * It takes an index i and makes sure that the subtree rooted at index i is a
   * min heap
   *
   * @param i index of the node
   */
  void minHeapify(int i) {
    int smallest = i, left = 2 * i + 1, right = 2 * i + 2;
    if (n > left && array[smallest].second > array[left].second) {
      smallest = left;
    }
    if (n > right && array[smallest].second > array[right].second) {
      smallest = right;
    }
    if (smallest != i) {
      Node smallestNode = array[smallest], iNode = array[i];
      pos[smallestNode.first] = i;
      pos[iNode.first] = smallest;
      swap(array[smallest], array[i]);
      minHeapify(smallest);
    }
  }

 public:
  /**
   * The function initializes the heap with a size of n and sets the first
   * element of the array to (0, 0)
   *
   * @param n The number of elements in the array.
   */
  Heap(int n) {
    init(n);
    array[0] = make_pair(0, 0);
  }

  /**
   * It creates a heap with the given keys.
   *
   * @param n the number of elements in the heap
   * @param key the array of keys
   */
  Heap(int n, vector<int>& key) {
    init(n);
    for (int i = 0; i < n; i++) {
      decreaseKey(i, key[i]);
    }
    extractMin();
  }

  /**
   * Verifies if the heap is empty.
   *
   * @return true if the heap is empty, false otherwise.
   */
  bool isEmpty() { return n == 0; }

  /**
   * We take the root node, swap it with the last node, and then call minHeapify
   * on the root node
   *
   * @return The first element of the root node.
   */
  int extractMin() {
    Node root = array[0], lastNode = array[n - 1];
    array[0] = lastNode;
    pos[root.first] = n - 1;
    pos[lastNode.first] = 0;
    n--;
    minHeapify(0);
    return root.first;
  }

  /**
   * The function takes in a vertex and a key value. It then finds the position
   * of the vertex in the heap and then swaps the vertex with its parent until
   * the heap property is restored
   *
   * @param v the vertex whose key is to be decreased
   * @param key the new key value
   */
  void decreaseKey(int v, int key) {
    int i = pos[v], j = (i - 1) / 2;
    array[i].second = key;
    while (i && array[j].second > array[i].second) {
      pos[array[j].first] = i;
      pos[array[i].first] = j;
      swap(array[j], array[i]);
      i = j;
      j = (i - 1) / 2;
    }
  }

  /**
   * If the number of elements in the heap is greater than the position of the
   * vertex in the heap, then the vertex is in the heap
   *
   * @param v The vertex whose position we want to check.
   *
   * @return true if the vertex is in the heap, false otherwise.
   */
  bool contains(int v) { return n > pos[v]; }

  vector<int> getPos() { return pos; }

  vector<Node> getArray() { return array; }
};

#endif  // HEAP_H_INCLUDED
