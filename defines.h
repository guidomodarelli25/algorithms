#ifndef GLOBAL_DEFINES
#define GLOBAL_DEFINES

#define iterator(v) decltype((v).begin())

#define foreach(it, v) for (decltype(v.begin()) it = v.begin(); it != v.end(); ++it)

#endif // GLOBAL_DEFINES