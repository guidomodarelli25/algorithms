#include <cstring>
#include <iostream>

using namespace std;

/**
 * It checks if the pattern is found in the text starting at position s
 *
 * @param text The text to search in.
 * @param pattern The pattern to search for.
 * @param s the starting index of the text string
 *
 * @return true if the pattern is found in the text, false otherwise
 */
bool has_match(char* text, char* pattern, int s) {
  for (unsigned i = 0; i < strlen(pattern); i++) {
    if (pattern[i] != text[s + i]) {
      return false;
    }
  }
  return true;
}

/**
 * It calculates the hash of the pattern and the hash of the first substring of
 * the text of length m. If the hashes are equal, it checks if there's a match.
 * If there's no match, it calculates the hash of the next substring of the text
 * of length m
 *
 * @param text The text to be searched.
 * @param pattern The pattern to be searched for.
 * @param d The number of characters in the input alphabet.
 * @param q A prime number.
 */
void Rabin_Karp(char* text, char* pattern, int d, int q) {
  /* Declaring and initializing the variables n, m, p, t, and h. */
  int n = strlen(text), m = strlen(pattern), p = 0, t = 0, h = 1;
  for (int i = 0; i < m - 1; i++) {
    h = (h * d) % q;
  }
  /* Calculating the hash of the pattern and the hash of the first substring of
   * the text of length m. */
  for (int i = 0; i < m; i++) {
    p = (d * p + pattern[i]) % q;
    t = (d * t + text[i]) % q;
  }
  for (int s = 0; s <= n - m; s++) {
    if (p == t) {
      if (has_match(text, pattern, s)) {
        cout << "Match en: " << s << "\n";
      }
    } else if (s < n - m) {
      /* Calculating the hash of the next substring of the text of length m. */
      t = (d * (t - text[s] * h) + text[s + m]) % q;
      if (t < 0) {
        t += q;
      }
    }
  }
}
