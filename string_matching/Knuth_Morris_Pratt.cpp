#include <cstring>
#include <iostream>

using namespace std;

/**
 * It finds the longest prefix that is also a suffix of the pattern
 *
 * @param pattern The pattern to search for.
 *
 * @return an array of integers.
 */
int* pre_calculate(char* pattern) {
  int m = strlen(pattern);
  int* f = new int[m];
  f[0] = -1;
  int i = -1, j = 0;
  while (j < m) {
    while (i > -1 && pattern[j] != pattern[i]) {
      i = f[i];
    }
    j++, i++;
    if (j < m) {
      f[j] = pattern[j] == pattern[i] ? f[i] : i;
    }
  }
  return f;
}

/**
 * We start with the first character of the pattern and the first character of
 * the text. If they match, we move to the next character of the pattern and the
 * next character of the text. If they don't match, we move to the next
 * character of the text and the next character of the pattern, but we also use
 * the failure function to determine how many characters of the pattern we can
 * skip
 *
 * @param text The text to be searched.
 * @param pattern the pattern to be searched for
 */
void Knuth_Morris_Pratt(char* text, char* pattern) {
  int n = strlen(text), m = strlen(pattern);
  int* f = pre_calculate(pattern);
  int i = 0, j = 0;
  while (j < n) {
    while (i > -1 && pattern[i] != text[j]) {
      i = f[i];
    }
    i++, j++;
    if (i >= m) {
      cout << "Match en: " << j - i << endl;
      i = f[i - 1];
    }
  }
}
