#include <cstring>
#include <iostream>

using namespace std;

/**
 * It returns true if the string "text" has a match with the string "pattern"
 * starting at position s
 *
 * @param text The text to search in.
 * @param pattern the pattern to search for
 * @param s the starting index of the text string
 *
 * @return true if the string "text" has a match with the string "pattern", false otherwise
 */
bool has_match(char* text, char* pattern, int s) {
  int n = strlen(pattern);
  for (int i = 0; i < n; i++) {
    if (pattern[i] != text[s + i]) {
      return false;
    }
  }
  return true;
}

/**
 * It compares the pattern with every substring of the text of the same length
 * as the pattern
 *
 * @param text The text to search in.
 * @param pattern the pattern to be searched for
 */
void naive_algorithm(char* text, char* pattern) {
  int n = strlen(text), m = strlen(pattern);
  for (int s = 0; s <= n - m; s++) {
    if (has_match(text, pattern, s)) {
      cout << "Match en" << s << endl;
    }
  }
}
