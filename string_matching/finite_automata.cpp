#include <cstring>
#include <iostream>

using namespace std;

#define NO_OF_CHARS 256

/**
 * It returns the longest prefix that is also a suffix of the pattern up to the
 * current character
 *
 * @param pattern The pattern that we are searching for.
 * @param m The length of the pattern.
 * @param j The current state of the automaton.
 * @param s The current character in the text.
 *
 * @return The next state.
 */
int get_next_state(char* pattern, int m, int j, int s) {
  /* Checking if the current character in the pattern is equal to the current
  character in the text. If it is, then it returns the next state. */
  if (j < m && s == pattern[j]) {
    return j + 1;
  }
  /* Finding the longest prefix that is also a suffix. */
  int k = j;
  while (k > 0) {
    int i = 0;
    if (pattern[k - 1] == s) {
      while (i < k - 1 && pattern[i] == pattern[j - k + 1 + i]) {
        i++;
      }
      if (i == k - 1) {
        return k;
      }
    }
    k--;
  }
  return 0;
}

/**
 * It builds a finite automata for a given pattern and then uses it to search
 * for the pattern in the text
 *
 * @param text The text in which we are searching for the pattern.
 * @param pattern The pattern that we are searching for.
 */
void finite_automata(char* text, char* pattern) {
  int n = strlen(text), m = strlen(pattern), Automata[m + 1][NO_OF_CHARS];
  /* Building the finite automata for a given pattern. */
  for (int j = 0; j <= m; j++) {
    for (int s = 0; s < NO_OF_CHARS; s++) {
      Automata[j][s] = get_next_state(pattern, m, j, s);
    }
  }
  /* This is the part of the code that is actually searching for the pattern in
  the text. */
  for (int i = 0, j = 0; i < n; i++) {
    j = Automata[j][(int)text[i]];
    if (j == m) {
      cout << "Match en: " << i - m + 1 << "\n";
    }
  }
}
