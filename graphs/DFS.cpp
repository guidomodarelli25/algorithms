#include <climits>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

#define VISITED 1
#define NOT_VISITED -1
#define DISCOVERED 0

vector<int> father, discovered, ending, state;
int time = 0;

void dfs(const Graph& g, int u) {
  state[u] = DISCOVERED;
  discovered[u] = ++time; /* also indicates the number of pre order */
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size(); i++) {
    int v = adj[i];
    if (state[v] == NOT_VISITED) {
      father[v] = u;
      dfs(g, v);
    }
  }
  state[u] = VISITED;
  ending[u] = ++time; /* also indicates the number of post order */
}

void dfs_forest(const Graph& g) {
  int n = g.size();
  discovered.resize(n, 0);
  ending.resize(n, 0);
  father.resize(n, INT_MAX);
  state.resize(n, NOT_VISITED);
  for (int u = 0; u < n; u++) {
    if (state[u] == NOT_VISITED) {
      dfs(g, u);
    }
  }
}

// dfs with edge classification

typedef pair<int, int> Arco;

void dfs(const Graph& g, int u, vector<int>& state, list<Arco>& back,
         list<Arco>& forward, list<Arco>& cross, vector<int>& discovered,
         int& time) {
  state[u] = DISCOVERED;
  discovered[u] = ++time;
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size(); i++) {
    int v = adj[i];
    if (state[v] == NOT_VISITED) {
      dfs(g, v, state, back, forward, cross, discovered, time);
    } else {
      Arco a = make_pair(u, v);
      if (state[v] == DISCOVERED) {
        back.push_back(a);
      } else {
        if (discovered[u] < discovered[v]) {
          forward.push_back(a);
        } else if (discovered[u] > discovered[v]) {
          cross.push_back(a);
        }
      }
    }
  }
  state[u] = VISITED;
}
