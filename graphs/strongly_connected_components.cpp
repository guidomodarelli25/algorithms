#include <algorithm>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"
#include "./topological_sort.cpp"

using namespace std;

list<list<int>> strongly_connected_components(const Graph& g) {
  list<list<int>> components;
  Graph gR;
  gR.reverse(g);
  int n = gR.size();
  list<int> stack = topological_sort(g);
  vector<bool> visited(n, false);
  while (!stack.empty()) {
    int u = stack.front();
    stack.pop_front();
    if (!visited[u]) {
      list<int> component;
      dfs(gR, u, visited, component);
      components.push_back(component);
    }
  }
  return components;
}
