#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

void dfs(const Graph& g, int u, vector<bool>& visited, list<int>& stack) {
  visited[u] = true;
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size(); i++) {
    int v = adj[i];
    if (!visited[v]) {
      dfs(g, v, visited, stack);
    }
  }
  stack.push_front(u);
}

list<int> topological_sort(const Graph& g) {
  list<int> stack;
  vector<bool> visited(g.size(), false);
  int n = g.size();
  for (int u = 0; u < n; u++) {
    if (!visited[u]) {
      dfs(g, u, visited, stack);
    }
  }
  return stack;
}
