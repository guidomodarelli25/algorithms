#include <algorithm>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

/*
 * Dados un grafo G con sus vertices rotulados con colores y dos vertices
 * v1 y v2, escriba un algoritmo que encuentre un camino entre el vertice v1 al
 * vertice v2 tal que no pase por vertices rotulados con el color rojo.
 */

void PathRojo(const Graph& g, set<int>& red, int v1, int v2) {
  if (red.find(v1) == red.end() && red.find(v2) == red.end()) {
    list<int> way;
    way.push_back(v1);
    list<list<int> > stack;
    stack.push_back(way);
    while (!stack.empty()) {
      way = stack.front();
      stack.pop_front();
      vector<int> adj = g.getAdjacentsVertices(way.back());
      for (unsigned i = 0; i < adj.size(); i++) {
        int w = adj[i];
        if (find(way.begin(), way.end(), w) == way.end() &&
            red.find(w) == red.end()) {
          way.push_back(w);
          if (w == v2) {
            return;
          } else {
            stack.push_back(way);
          }
          way.pop_back();
        }
      }
    }
  }
}
