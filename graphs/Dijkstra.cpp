#include <climits>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"
#include "../collections/Heap.cpp"

using namespace std;

vector<int> dist, father;

int ExtractMinimumVertex(list<int>& vertices) {
  int minimum = INT_MAX, w = 0;
  foreach (u, vertices) {
    if (minimum > dist[*u]) {
      minimum = dist[*u];
      w = *u;
    }
  }
  return w;
}

void dijkstra(const Graph& g, int u) {
  int n = g.size();
  dist.resize(n, INT_MAX);
  father.resize(n, INT_MAX);
  list<int> solution, vertices = g.getVertices();
  solution.push_back(u);
  vertices.remove(u);
  dist[u] = 0;
  list<Graph::Arch> adjacents = g.getAdjacents(u);
  for (list<Graph::Arch>::iterator it = adjacents.begin();
       it != adjacents.end(); it++) {
    int v = it->getAdjacent();
    dist[v] = it->getCost();
    father[v] = u;
  }
  while (!vertices.empty()) {
    int w = ExtractMinimumVertex(vertices);
    solution.push_back(w);
    vertices.remove(w);
    list<Graph::Arch> adjacents = g.getAdjacents(w);
    for (list<Graph::Arch>::iterator it = adjacents.begin();
         it != adjacents.end(); it++) {
      int v = it->getAdjacent();
      if (dist[v] > dist[w] + it->getCost()) {
        dist[v] = dist[w] + it->getCost();
        father[v] = w;
      }
    }
  }
}

/******************************************************************************************************************/

/* Dijkstra implementado con heap */

vector<int> dist, father;

typedef pair<int, int> Vertice;

void Dijkstra(vector<vector<int> >& Matriz, int u) {
  int n = Matriz.size();
  dist = Matriz[u];
  father.resize(n, u);
  Heap minHeap(n, dist);
  while (!minHeap.isEmpty()) {
    int w = minHeap.extractMin();
    for (int v = 0; v < n; v++) {
      if (Matriz[w][v] != INT_MAX) {
        if (dist[v] > dist[w] + Matriz[w][v]) {
          dist[v] = dist[w] + Matriz[w][v];
          father[v] = w;
          minHeap.decreaseKey(v, dist[w] + Matriz[w][v]);
        }
      }
    }
  }
}
