#include <algorithm>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

/*
 * Escriba un algoritmo que devuelva una lista con todos los vertices de un
 * grafo dirigido, a partir de los cuales existe un camino de longitud "long"
 * que termina en un vertice dado.
 */

bool DFS_Paths(const Graph& g, int u, int v, int n) {
  list<int> way;
  way.push_back(u);
  list<list<int> > stack;
  stack.push_back(way);
  while (!stack.empty()) {
    way = stack.front();
    stack.pop_front();
    vector<int> adj = g.getAdjacentsVertices(way.back());
    for (unsigned i = 0; i < adj.size(); i++) {
      int w = adj[i];
      if (find(way.begin(), way.end(), w) == way.end()) {
        way.push_back(w);
        if (w == v) {
          int m = way.size();
          if (m == n) {
            return true;
          }
        } else {
          stack.push_back(way);
        }
        way.pop_back();
      }
    }
  }
  return false;
}

list<int> PathLength(const Graph& g, int n, int v) {
  list<int> out;
  int m = g.size();
  for (int u = 0; u < m; u++) {
    if (DFS_Paths(g, u, v, n)) {
      out.push_back(u);
    }
  }
  return out;
}
