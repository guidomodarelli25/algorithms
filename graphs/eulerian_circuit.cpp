#include <algorithm>
#include <list>
#include <set>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

typedef pair<int, int> Edge;

bool cycle(const Graph& g, int u, int first, set<Edge>& edges,
           list<int>& vertices) {
  vertices.push_back(u);
  bool found = false;
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size(); i++) {
    int v = adj[i];
    // Si la arista no habia sido considerada
    if (edges.find(make_pair(u, v)) != edges.end()) {
      edges.erase(make_pair(u, v));
      edges.erase(make_pair(v, u));
      if (v != first) {
        found = cycle(g, v, first, edges, vertices);
      } else {
        found = true;
      }
      if (found) {
        break;
      }
    }
  }
  return found;
}

list<int> eulerian_circuit(const Graph& g) {
  list<int> vertices;
  vertices.push_back(0);
  set<Edge> edges = g.getSetEdges();
  bool thereIsCircuit = true;
  for (set<Edge>::iterator it = edges.begin(); !edges.empty() && thereIsCircuit;
       it++) {
    list<int>::iterator itV = find(vertices.begin(), vertices.end(), it->first);
    if (itV != vertices.end()) {
      list<int> aux;
      thereIsCircuit = cycle(g, it->first, it->first, edges, aux);
      if (thereIsCircuit) {
        vertices.insert(itV, aux.begin(), aux.end());
      } else {
        vertices.clear();
      }
    }
  }
  return vertices;
}
