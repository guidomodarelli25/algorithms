#include <climits>
#include <list>
#include <vector>

#include "./Floyd.cpp"

using namespace std;

/*
 * Desde un cierto numero k de ciudades del interior de una provincia, se
 * desean transportar cereales hasta algun port perteneciente al litoral de
 * la misma. Se pretende efectuar el transporte con minimum costo. De un
 * algoritmo eficiente que resuelva este problema, devolviendo los caminos en
 * una estructura de lista.
 */

list<list<int> > minimumTransportationCost(vector<vector<int> >& A,
                                           vector<bool>& ports) {
  list<list<int> > ways;
  int n = A.size();
  vector<vector<int> > P(n, vector<int>(n, 0));
  floyd(A, P);
  for (int ciudad = 0; ciudad < n; ciudad++) {
    int minimum = INT_MAX, port;
    // obtengo el camino de la ciudad al puerto mas cercano
    if (!ports[ciudad]) {
      for (int u = 0; u < n; u++) {
        if (ports[u]) {
          if (minimum > A[ciudad][u]) {
            minimum = A[ciudad][u];
            port = u;
          }
        }
      }
    }
    list<int> way;
    way.push_back(ciudad);
    camino(P, ciudad, port, way);
    way.push_back(port);
    ways.push_back(way);
  }
  return ways;
}
