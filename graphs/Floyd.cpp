#include <list>
#include <vector>

using namespace std;

void floyd(vector<vector<int> >& A, vector<vector<int> >& P) {
  int n = A.size();
  for (int k = 1; k < n; k++) {
    for (int i = 1; i < n; i++) {
      for (int j = 1; j < n; j++) {
        if (A[i][j] > A[i][k] + A[k][j]) {
          A[i][j] = A[i][k] + A[k][j];
          P[i][j] = k;
        }
      }
    }
  }
}

void camino(vector<vector<int> >& P, int i, int j, list<int>& way) {
  int k = P[i][j];
  if (k > 0) {
    camino(P, i, k, way);
    way.push_back(k);
    camino(P, k, j, way);
  }
}
