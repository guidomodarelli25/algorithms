#include <list>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

typedef pair<int, int> Arista;

bool hamiltonian_cycle(const Graph& g, int u, int first, unsigned k,
                      vector<bool>& visited, list<Arista>& edges) {
  visited[u] = true;
  if (k == g.size()) {
    return true;
  } else {
    bool found = false;
    vector<int> adj = g.getAdjacentsVertices(u);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      if (!visited[v] || v == first) {
        edges.push_back(make_pair(u, v));
        found = hamiltonian_cycle(g, v, first, k + 1, visited, edges);
        if (found) {
          break;
        }
        edges.pop_back();
        visited[v] = false;
      }
    }
    return found;
  }
}
