#include <vector>

#include "../collections/Graph.cpp"
#include "./DFS.cpp"

using namespace std;

bool single_cycle(const Graph& g, int u, int first, vector<bool>& visited) {
  visited[u] = true;
  bool found = false;
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size() && !found; i++) {
    int v = adj[i];
    if (!visited[v]) {
      found = single_cycle(g, v, first, visited);
    } else if (v == first) {
      /* (nro + 1) % 3 == 0 longitud multiplo de 3 */
      /* Long = i + 1 longitud Long */
      return true;
    }
  }
  return found;
}

bool cyclicity_test(const Graph& g) {
  int n = g.size();
  vector<int> postOrder(n, 0);
  dfs_forest(g);
  postOrder = discovered;
  for (int u = 0; u < n; u++) {
    vector<int> adj = g.getAdjacentsVertices(u);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      if (postOrder[v] >= postOrder[u]) {
        return true;
      }
    }
  }
  return false;
}
