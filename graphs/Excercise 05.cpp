#include <algorithm>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

/*
 * Escribir un algoritmo que, dado un grafo dirigido y dos vertices de este
 * grafo, devuelva todos los caminos simples de un vertice a otro.
 */

list<list<int> > DFS_Paths(const Graph& g, int u, int v) {
  list<int> way;
  way.push_back(u);
  list<list<int> > stack, out;
  stack.push_back(way);
  while (!stack.empty()) {
    way = stack.front();
    stack.pop_front();
    vector<int> adj = g.getAdjacentsVertices(way.back());
    for (unsigned i = 0; i < adj.size(); i++) {
      int w = adj[i];
      if (find(way.begin(), way.end(), w) == way.end()) {
        way.push_back(w);
        if (w == v) {
          out.push_back(way);
        } else {
          stack.push_back(way);
        }
        way.pop_back();
      }
    }
  }
  return out;
}
