#include <climits>
#include <set>
#include <vector>

#include "../collections/Graph.cpp"

using namespace std;

typedef pair<int, int> Arch;

int minimum(set<Arch>& s, int u, vector<int>& v) {
  int minimum = INT_MAX;
  for (set<Arch>::iterator it = s.begin(); it != s.end(); it++) {
    if (it->first == u) {
      minimum = min(minimum, v[it->second]);
    }
  }
  return minimum;
}

void articulation_points(const Graph& g, int u, int r, vector<bool>& visited,
                        vector<int>& preOrder, int& time, vector<int>& higher,
                        vector<int>& childs, set<Arch>& back,
                        set<Arch>& archesTree, set<int>& points) {
  visited[u] = true;
  preOrder[u] = ++time;
  vector<int> adj = g.getAdjacentsVertices(u);
  for (unsigned i = 0; i < adj.size(); i++) {
    int x = adj[i];
    if (!visited[x]) {
      childs[u]++;
      archesTree.insert(make_pair(u, x));
      articulation_points(g, x, r, visited, preOrder, time, higher, childs, back,
                         archesTree, points);
      if (u != r && childs[u] == 1) {
        if (higher[x] >= preOrder[u]) {
          points.insert(u);
        }
      }
    } else if (archesTree.find(make_pair(x, u)) == archesTree.end()) {
      // If it is not a dfs tree edge then it is a back edge
      back.insert(make_pair(u, x));
    }
  }
  // min(preOrder[u], preOrder[w], preOrder[x])
  higher[u] = min(preOrder[u], min(minimum(back, u, preOrder),
                                   minimum(archesTree, u, higher)));
  if (childs[u] > 1) {
    points.insert(u);
  }
}
