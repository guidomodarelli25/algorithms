#include <algorithm>
#include <list>
#include <vector>

#include "../collections/Graph.cpp"
#include "../collections/Set.cpp"

using namespace std;

void connected_components(const Graph& g) {
  int n = g.size();
  Set f(n);
  for (int u = 0; u < n; u++) {
    vector<int> adj = g.getAdjacentsVertices(u);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      int repU = f.find(u), repV = f.find(v);
      if (repU != repV) {
        f.union_(repU, repV);
      }
    }
  }
}
