#include <set>

#include "../collections/Graph.cpp"
#include "../collections/Set.cpp"

using namespace std;

typedef pair<int, int> Arch;

set<Arch> Kruskal(const Graph& g) {
  set<Arch> T;
  list<Arch> edges = g.getOrderedEdges();
  Set s(g.size());
  while (!edges.empty()) {
    Arch edge = edges.front();
    edges.pop_front();
    int repU = s.find(edge.first), repV = s.find(edge.second);
    if (repU != repV) {
      s.union_(repU, repV);
      T.insert(edge);
    }
  }
  return T;
}
