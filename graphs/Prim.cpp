#include <climits>
#include <list>
#include <set>
#include <vector>

#include "../collections/Graph.cpp"
#include "../collections/Heap.cpp"

using namespace std;

/* Implemented using Matrix */

typedef pair<int, int> Edge;

set<Edge> Prim(vector<vector<int> >& M) {
  set<Edge> T;
  int n = M.size(), visited = 1;
  vector<int> neighbour(n, 0), minimumCost(n);
  for (int i = 1; i < n; i++) {
    minimumCost[i] = M[i][0];
  }
  while (visited < n) {
    int k = 0, minimum = INT_MAX;
    for (int j = 1; j < n; j++) {
      if (0 <= minimumCost[j] && minimumCost[j] < minimum) {
        minimum = minimumCost[j];
        k = j;
      }
    }
    T.insert(make_pair(k, neighbour[k]));
    visited++;
    minimumCost[k] = -1;
    for (int j = 1; j < n; j++) {
      if (M[k][j] < minimumCost[j]) {
        minimumCost[j] = M[k][j];
        neighbour[j] = k;
      }
    }
  }
  return T;
}

/******************************************************************************************************************/

/* Implemented using Heap */

typedef pair<int, int> Edge;

set<Edge> Prim(const Graph& g) {
  set<Edge> out;
  int n = g.size();
  vector<int> parent(n, -1), key(n, INT_MAX);
  Heap minHeap(n);
  while (!minHeap.isEmpty()) {
    int u = minHeap.extractMin();
    list<Graph::Arch> adjacents = g.getAdjacents(u);
    for (list<Graph::Arch>::iterator it = adjacents.begin();
         it != adjacents.end(); it++) {
      int v = it->getAdjacent();
      if (minHeap.contains(v) && key[v] > it->getCost()) {
        key[v] = it->getCost();
        if (parent[v] != -1) {
          out.erase(make_pair(parent[v], v));
        }
        out.insert(make_pair(u, v));
        parent[v] = u;
        minHeap.decreaseKey(v, key[v]);
      }
    }
  }
  return out;
}
