#include <climits>
#include <vector>

#include "../collections/Graph.cpp"

vector<int> father, distance_;

void BFS(const Graph& g, int s) {
  vector<bool> visited(g.size(), false);
  father.resize(g.size(), INT_MAX);
  distance_.resize(g.size(), INT_MAX);
  visited[s] = true;
  distance_[s] = 0;
  list<int> queue;
  queue.push_back(s);
  while (!queue.empty()) {
    int u = queue.front();
    queue.pop_front();
    vector<int> adj = g.getAdjacentsVertices(u);
    for (unsigned i = 0; i < adj.size(); i++) {
      int v = adj[i];
      if (!visited[v]) {
        visited[v] = true;
        distance_[v] = distance_[u] + 1;
        father[v] = u;
        queue.push_front(v);
      }
    }
  }
}
